package evets.neg.ballspeed;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Steve on 5/18/2015.
 */

/**
 * Generic Adapter used to display a row in a ListView.  It has 1 Main field for displaying and up to 5 generic fields
 * which typically will be hidden.
 */
public class AdapterGenericWithCodes extends BaseAdapter
{

    /****** Handle Interfaces **********************************************************************************************/

    private int lastClickedLinkID;

    // Calling activity/fragment can implement this interface to get a click event returned to them for an control with click..
    public interface OnViewClick {

        //handle on click for the view.  position is row in LV, view is view that was clicked, lvRow is the whole Row with all controls.
         void onClick(int position, View lvRow, View view);
    }

    //view listener
    private OnViewClick mOnViewListener;

     //Call this method to set the listener for when the view that is clicked
    public void setOnViewClickListener(OnViewClick mListener) {

        mOnViewListener = mListener;
    }

    //Get listener
    public OnViewClick getOnViewClickListener() {
        return mOnViewListener;
    }

     //Call this method to set the listener for when the view that is clicked
    public void setLastClickedViewID(int id) {

        lastClickedLinkID = id;
    }

    //Get listener
    public int getLastClickedViewID() {
        return lastClickedLinkID;
    }


    /**********************************************************************************************************************/

    private Context mContext;
    private String[] items;
    private String[][] codes = new String[10][];
    private int[] rowLayouts;
    private int[] dividerLayouts;

    private int lstRowlayout;
    private int lstRowGroupLayout;
    private int lstRowGroupHeaderLayout;

    private boolean[] positionState;
    private boolean[] clickable;
    private int intMainFieldGravity;
    private int intMainFieldMinHeight;
    private float mainFieldTextSize;
    private SparseBooleanArray checked;

    private enum CodesUsed {Five, Ten};
    private CodesUsed codesUsed;

    private boolean flRowLayoutArrayUsed;
    private boolean flDividerLayoutArrayUsed;

    //Capture filds to support a group column heading
    public enum GroupBy {Code1, Code2, Code3, Code4, Code5, code6, code7, code8, code9, code10};
    private boolean isGroupBySet;
    private boolean isDividerSet;

    private class GroupByWithRow {
        boolean rowIsGroupHeader;
        boolean rowIsListViewHeader;
        String rowText;
        int lstRowLayout;
        int arraysIndex;
        String alternateText1;
        String alternateText2;
        String alternateText3;

    }

    private ArrayList<GroupByWithRow> itemsWithGroups = new ArrayList<GroupByWithRow>();

    private boolean headerRowAdded;

    public static int ITEM_NOT_FOUND = -1;
    public static int IGNORE_PASSED_VALUE = -1;

    //Assume no min height for MainField.  Use what is in XML Layout
    private static final int MIN_HEIGHT_NO_VALUE = -1;

    //Assume no height for MainField.  Use what is in XML Layout
    private static final int TEXT_SIZE_NO_VALUE = -1;

    /**
     * getContext
     * @return  - context
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * return index of Items given the listbos row.  This may return -1 if row is header or group row
     * @param index
     * @return
     */
    public  int getArrayIndexFromGroup(int index) {
        return itemsWithGroups.get(index).arraysIndex;
    }

    /**
     * Set size of mainFIeld.  If not passed in, use XML value
     * @param size
     */
    public void setMainFieldTextSize(float size) {
        mainFieldTextSize = size;
    }

    /**
     * Add a header to the listview. May be called multiple times to add a header
     * @param headerText - text for header
     * @param layout - layout to use
     */
    public void addHeaderRow(String headerText, int layout) {

        //this.lstRowGroupHeaderLayout = layout;
        addHeaderRow(headerText, layout, null, null, null);

    }

    /**
     * Add a header to the listview. May be called multiple times to add a header.  May also add alternate text strings to be displayed in layout
     * @param headerText - text for header
     * @param layout - layout to use
     * @param alternateText1 - String to be displayed in layout for text field with ID of AlternateText1, AlternateText2, AlternateText3
     */
    public void addHeaderRow(String headerText, int layout, String alternateText1, String alternateText2, String alternateText3) {

        //this.lstRowGroupHeaderLayout = layout;

        //insert a non-enabled row in the Headerfield.
        GroupByWithRow tmpGrpRow = new GroupByWithRow();
        tmpGrpRow.rowIsGroupHeader = true;
        tmpGrpRow.rowIsListViewHeader = true;
        tmpGrpRow.arraysIndex = -1;
        tmpGrpRow.rowText = headerText;
        tmpGrpRow.lstRowLayout = layout;
        tmpGrpRow.alternateText1 = alternateText1;
        tmpGrpRow.alternateText2 = alternateText2;
        tmpGrpRow.alternateText3 = alternateText3;

        //find next available header row to add
        int startHeader = 0;

        if (itemsWithGroups.size() > 0)
            while (itemsWithGroups.get(startHeader).rowIsListViewHeader)
                startHeader++;

        itemsWithGroups.add(startHeader, tmpGrpRow);

        //Establish the position and clickable arrays
        loadPositionAndClickable();

        //set flag to be used in getView to show correct context
        headerRowAdded = true;


    }

    /**
     * Add a footer to the listview. May be called multiple times to add a footer
     * @param headerText - text for header
     * @param layout - layout to use
     */
    public void addFooterRow(String headerText, int layout) {

        //this.lstRowGroupHeaderLayout = layout;

        //insert a non-enabled row in the Headerfield.
        GroupByWithRow tmpGrpRow = new GroupByWithRow();
        tmpGrpRow.rowIsGroupHeader = true;
        tmpGrpRow.rowIsListViewHeader = true;
        tmpGrpRow.arraysIndex = -1;
        tmpGrpRow.rowText = headerText;
        tmpGrpRow.lstRowLayout = layout;

        //find end of rows to add footer
        int startHeader = getCount();

        //if (itemsWithGroups.size() > 0)
        //    while (itemsWithGroups.get(startHeader).rowIsListViewHeader)
        //        startHeader++;

        itemsWithGroups.add(startHeader, tmpGrpRow);

        //Establish the position and clickable arrays
        loadPositionAndClickable();

        //set flag to be used in getView to show correct context
        headerRowAdded = true;


    }

    /**
     * Removes a  row, starting from the bottom.  It will subtract the offset passed.
     * @param offset .  Value to subtract to remove a.  Zero is last bottom footer.  1 would be footer above bottom footer.  etc.
     */
    public void removeRowStartingFromBottom(int offset) {

        itemsWithGroups.remove(getCount() - 1 - offset);

        //Establish the position and clickable arrays
        loadPositionAndClickable();


    }

    /**
     * Returns whether a row is a header row
     * @param pos - position to validate
     * @return true is a row.  false if otherwise
     */
    public boolean isRowAHeader(int pos) {

        if (itemsWithGroups.get(pos).rowIsListViewHeader)
            return true;
        else
            return false;
    }

    /**
     * Update a header text at passed position.  If it does not exist, add a row at the next available spot
     * @param headerPosition = zero based header position.  If position is not a headerrow it is ignored
     * @param newText - new Text value
     */
    public void updateOrAddHeader(int headerPosition, String newText, int layout) {

        //if passed row is a header, update text
        if (itemsWithGroups.size() > 0 && itemsWithGroups.get(headerPosition).rowIsListViewHeader) {

            itemsWithGroups.get(headerPosition).rowText = newText;

            //if layout is passed, update that too
            if (layout != IGNORE_PASSED_VALUE)

                itemsWithGroups.get(headerPosition).rowText = newText;
        }

        //otherwise, add a row
        else
            if (layout != IGNORE_PASSED_VALUE)
                addHeaderRow(newText, layout);
        }

    /**
     * Creates the list of items to show in listview or spinner, including header rows
     * @param groupByField1 - required, but may be null.  null means no grouping
     * @param groupByField2 - required, but may be null.  If null, method  will only group by one field
     */
    private void loadItemsWithGroupsArrayList(String[] groupByField1, String[] groupByField2) {

        if (items == null)
            return;

        //store last Group value
        String lastGroup = null;

        //initiatlize old ArrayList
        itemsWithGroups.clear();

        //Now iterate through all items.   this routine assumes thr groupBy field is selected.
        //create a master list which will be used for display, and another array tracking a group field
        //or data field.

        GroupByWithRow grpRow = new GroupByWithRow();

        //Assume very first generic field is first group or Divider row
        if (isGroupBySet) {
            grpRow.rowIsGroupHeader = true;

            if (groupByField2 != null)
                grpRow.rowText = lastGroup = groupByField1[0] + " " +  groupByField2[0];
            else
                grpRow.rowText = lastGroup = groupByField1[0];

            grpRow.lstRowLayout = lstRowGroupLayout;

            grpRow.arraysIndex = -1;
            itemsWithGroups.add(grpRow);
        }

        int i=0;
        while (i<items.length){

            grpRow = new GroupByWithRow();
            String valToCompare = null;

            //depending on if one group or two groups are passed in, determine value to compare from prior row
            if (isGroupBySet)
                if (groupByField2 == null)
                valToCompare = groupByField1[i];
            else
                valToCompare = groupByField1[i] + " " + groupByField2[i];


            //if (isGroupBySet && (!lastGroup.equals(groupByField1[i]) || ( groupByField2 != null && !lastGroup.equals(groupByField2[i])))) {
            if (isGroupBySet &&  !lastGroup.equals(valToCompare)) {

                grpRow.rowIsGroupHeader = true;

                if (groupByField2 != null)
                    grpRow.rowText = lastGroup = groupByField1[i] + " " + groupByField2[i];
                else
                    grpRow.rowText = lastGroup = groupByField1[i];

                grpRow.lstRowLayout = lstRowGroupLayout;

                grpRow.arraysIndex = -1;
                itemsWithGroups.add(grpRow);

            }
            else {

                //If a divider is set and it is not null, add it.
                if (isDividerSet && groupByField1[i] != null) {

                    grpRow.rowIsGroupHeader = true;
                    grpRow.rowText = groupByField1[i];
                    grpRow.arraysIndex = -1;
                    itemsWithGroups.add(grpRow);

                    if (!flDividerLayoutArrayUsed)
                        grpRow.lstRowLayout = lstRowGroupLayout;
                    else
                        grpRow.lstRowLayout = dividerLayouts[i];

                    //create new group.
                    grpRow = new GroupByWithRow();
                }

                //If an array of layouts was not passed, assume a single layout constructor was used
                if (!flRowLayoutArrayUsed)
                    grpRow.lstRowLayout = lstRowlayout;

                //Otherwise an array was used, so set layout for each row
                else
                    grpRow.lstRowLayout = rowLayouts[i];

                //now add the regular row
                grpRow.rowIsGroupHeader = false;
                grpRow.rowText = items[i];
                grpRow.arraysIndex = i;
                itemsWithGroups.add(grpRow);
                i++;
            }

        }

    }

    /**
     * Set the field to GroupBy.
     * @param grp - field to group by
     * @param layout - layout to use as header
     */
    public void setGroupField(GroupBy grp, int layout) {

        this.lstRowGroupLayout = layout;

        //Group By is on
        isGroupBySet = true;

        //load the itemsWItGroup ArrayList.  Specify array code to use as the GroupBy.

        if(codes[grp.ordinal()] != null && codes[grp.ordinal()].length > 0) {
            loadItemsWithGroupsArrayList(codes[grp.ordinal()], null);
            loadPositionAndClickable();
        }

    }

    /**
     * Set the divider field.  This is a row that is not clickable that will be placed between every row.  If null is passed, the row will be skipped.
     * This is another way to add group by fields, but possibly have different text values.  This cannot be used with setGroupBy field.
     * @param grp - field to place between every row
     * @param layout - layout to use as header
     */
    public void setDividerField(GroupBy grp, int layout) {

        this.lstRowGroupLayout = layout;

        //Group By is on
        isDividerSet = true;
        flDividerLayoutArrayUsed = false;

        //load the itemsWItGroup ArrayList.  Specify array code to use as the GroupBy.

        if(codes[grp.ordinal()] != null && codes[grp.ordinal()].length > 0) {
            loadItemsWithGroupsArrayList(codes[grp.ordinal()], null);
            loadPositionAndClickable();
        }

    }

    /**
     * Set the divider field.  This is a row that is not clickable that will be placed between every row.  If null is passed, the row will be skipped.
     * This is another way to add group by fields, but possibly have different text values.  This cannot be used with setGroupBy field.
     * @param grp - field to place between every row
     * @param layout - layout to use as header
     */
    public void setDividerField(GroupBy grp, int[] layout) {

        dividerLayouts = layout;

        //Group By is on
        isDividerSet = true;
        flDividerLayoutArrayUsed = true;

        //load the itemsWItGroup ArrayList.  Specify array code to use as the GroupBy.

        if(codes[grp.ordinal()] != null && codes[grp.ordinal()].length > 0) {
            loadItemsWithGroupsArrayList(codes[grp.ordinal()], null);
            loadPositionAndClickable();
        }

    }

    /**
     * Set the field to GroupBy.
     * @param grp1 - field 1 to group by
     * @param grp2 - field 2 to group by
     * @param layout - layout to use as header
     */
    public void setGroupField(GroupBy grp1, GroupBy grp2, int layout) {

        this.lstRowGroupLayout = layout;

        //Group By is on
        isGroupBySet = true;

        //load the itemsWItGroup ArrayList.  Specify array code to use as the GroupBy.
        if(codes[grp1.ordinal()] != null && codes[grp1.ordinal()].length > 0 && codes[grp2.ordinal()] != null && codes[grp2.ordinal()].length > 0) {

            loadItemsWithGroupsArrayList(codes[grp1.ordinal()], codes[grp2.ordinal()]);
            loadPositionAndClickable();
        }
    }


    private void initializeHeaders() {

        //Assume no group by initially and load the itemsWithGroups ArrayList wih only items, no groups.
        //if Groups are wanted, caller should call setGroupBy method with passed enum GroupBy
        //indicating code to Group By.
        isGroupBySet = false;
        loadItemsWithGroupsArrayList(null, null);
        loadPositionAndClickable();

        //Assume no header row
        headerRowAdded = false;

        checked = new SparseBooleanArray();

    }
    /**
     *
     * @param context
     * @param items
     * @param code1
     * @param code2
     * @param code3
     * @param code4
     * @param code5
     * @param code6
     * @param code7
     * @param code8
     * @param code9
     * @param code10
     * @param lstRowLayout
     */
    public AdapterGenericWithCodes(Context context, String[] items, String[] code1, String[] code2, String[] code3, String[] code4, String[] code5,
                                   String[] code6, String[] code7, String[] code8, String[] code9, String[] code10, int lstRowLayout) {

        super();
        this.mContext = context;
        this.items = items;
        this.lstRowlayout = lstRowLayout;

        this.codes[0] = code1;
        this.codes[1] = code2;
        this.codes[2] = code3;
        this.codes[3] = code4;
        this.codes[4] = code5;
        this.codes[5] = code6;
        this.codes[6] = code7;
        this.codes[7] = code8;
        this.codes[8] = code9;
        this.codes[9] = code10;

        //default gravity for mainFied
        intMainFieldGravity = Gravity.NO_GRAVITY;

        //default gravity for mainFied
        intMainFieldMinHeight = MIN_HEIGHT_NO_VALUE;

        //No passed value for mainfield
        mainFieldTextSize = TEXT_SIZE_NO_VALUE;

        //Array of layouts were not passed, so use same layout for all rows;
        flRowLayoutArrayUsed = false;

        initializeHeaders();

        codesUsed = CodesUsed.Ten;

    }

    /**
     *
     * @param context
     * @param items
     * @param code1
     * @param code2
     * @param code3
     * @param code4
     * @param code5
     * @param code6
     * @param code7
     * @param code8
     * @param code9
     * @param code10
     * @param lstRowLayout
     */
    public AdapterGenericWithCodes(Context context, String[] items, String[] code1, String[] code2, String[] code3, String[] code4, String[] code5,
                                   String[] code6, String[] code7, String[] code8, String[] code9, String[] code10, int[] lstRowLayout) {

        super();
        this.mContext = context;
        this.items = items;
        this.rowLayouts = lstRowLayout;

        this.codes[0] = code1;
        this.codes[1] = code2;
        this.codes[2] = code3;
        this.codes[3] = code4;
        this.codes[4] = code5;
        this.codes[5] = code6;
        this.codes[6] = code7;
        this.codes[7] = code8;
        this.codes[8] = code9;
        this.codes[9] = code10;

        //default gravity for mainFied
        intMainFieldGravity = Gravity.NO_GRAVITY;

        //default gravity for mainFied
        intMainFieldMinHeight = MIN_HEIGHT_NO_VALUE;

        //No passed value for mainfield
        mainFieldTextSize = TEXT_SIZE_NO_VALUE;

        //Array of layouts were  passed, so use same different layout for all rows;
        flRowLayoutArrayUsed = true;

        initializeHeaders();

        codesUsed = CodesUsed.Ten;

    }

    /**
     * Used to add a single row of up to 10 values.  This may be called AFTER the constructor is called with 5 or 10 generic values.  If the constructor is called
     * with 5 values and more than five is passed, this may not work corectly.
     * Although this will add a single row, it doesnot put intp the right grouping.   This will also overright heads and groups, so they will have to be re-called after using
     * this mthod.  Basically, this is a good method to use for un-ordered, un-grouped, un-headed listviews and spinners.
     * @param item
     * @param code1
     * @param code2
     * @param code3
     * @param code4
     * @param code5
     * @param code6
     * @param code7
     * @param code8
     * @param code9
     * @param code10
     * @param lstRowLayout
     */

    public void addSingleRow(String item, String code1, String code2, String code3, String code4, String code5,
                                   String code6, String code7, String code8, String code9, String code10, int lstRowLayout) {


        //Store new item
        if (items == null) {
            items = new String[1];
            items[0] = item;
        } else {
            items = Arrays.copyOf(items, items.length + 1);
            items[items.length - 1] = item;
        }

        //Store codes
        for(int i=0; i<10; i++) {

            String retCode = getCode(i, code1, code2, code3, code4, code5, code6, code7, code8, code9, code10);

            if (retCode != null)
                if (codes[i] == null) {
                    String[] tempArray = {retCode};
                    codes[i] = tempArray;
                } else {
                    codes[i] = Arrays.copyOf(codes[i], codes[i].length + 1);
                    codes[i][codes[i].length - 1] = retCode;
                }

        }

        //Record the new layout
        this.lstRowlayout = lstRowLayout;

        loadItemsWithGroupsArrayList(null, null);
        loadPositionAndClickable();

    }

    //Based on the passed number, return the code String
    private String getCode(int number, String code1, String code2, String code3, String code4, String code5,
                           String code6, String code7, String code8, String code9, String code10) {

        String ret = null;

        switch (number+1) {

            case 1:
                ret = code1;
                break;
            case 2:
                ret = code2;
                break;
            case 3:
                ret = code3;
                break;
            case 4:
                ret = code4;
                break;
            case 5:
                ret = code5;
                break;
            case 6:
                ret = code6;
                break;
            case 7:
                ret = code7;
                break;
            case 8:
                ret = code8;
                break;
            case 9:
                ret = code9;
                break;
            case 10:
                ret = code10;
                break;
        }

        return ret;
    }


    /**
    ** Call this constructor will up to five parameters:
    **      @param items - the value to display in the listbox
    **      @param code1 - typically hidden values for row reference
    **      @param code2 - typically hidden values for row reference
    **      @param code3 - typically hidden values for row reference
    **      @param code4 - typically hidden values for row reference
    **      @param code5 - typically hidden values for row reference
    **      @param lstRowLayout - xml rowLayout to inflate in listrow (use generic list_row_generic_5_codes.xml already built)
*/
    public AdapterGenericWithCodes(Context context, String[] items, String[] code1, String[] code2, String[] code3, String[] code4, String[] code5, int lstRowLayout) {
        super();
        this.mContext = context;
        this.items = items;
        this.lstRowlayout = lstRowLayout;

        this.codes[0] = code1;
        this.codes[1] = code2;
        this.codes[2] = code3;
        this.codes[3] = code4;
        this.codes[4] = code5;

        //default gravity for mainFied
        intMainFieldGravity = Gravity.NO_GRAVITY;

        //default min height for mainFied
        intMainFieldMinHeight = MIN_HEIGHT_NO_VALUE;

        //No passed value for mainfield
        mainFieldTextSize = TEXT_SIZE_NO_VALUE;

        //Array of layouts were not passed, so use same layout for all rows;
        flRowLayoutArrayUsed = false;

        initializeHeaders();

        codesUsed = CodesUsed.Five;

    }

    private void loadPositionAndClickable() {

        //Assume all rows in listview are normal and selectable
        int l;
        if (itemsWithGroups == null || itemsWithGroups.size() == 0)
            l = 0;
        else
            l = itemsWithGroups.size();

        positionState = new boolean[l];
        clickable = new boolean[l];
        for (int i = 0; i < positionState.length; i++) {
            positionState[i] = true;
            clickable[i] = true;
        }

    }

    /**
     * Set the Gravity of the Mainfield.
     * @param g
     */
    public void setMainFieldGravity(int g) {

        intMainFieldGravity = g;
    }

    /**
     * Set the minHeight of the Mainfield.
     * @param m
     */
    public void setMainFieldMinHeight(int m) {

        intMainFieldMinHeight = m;
    }

    /**
    ** setPositionState() - used to tell adapter when viewing fields how to display fields at position
    ** and how if row is clickable:
    **
    **      enabled     clickable   result
    **      -------     ---------   ------
    **      true        true        row will be normal color, and selectable
    **      false       true        row will be gray and selectable
    **      true        false       row will be normal color, and not selectable
    **      false       false       row will be gray and npt selectable
     */
    public void setPositionState(int position, boolean enabled, boolean clickable) {


        positionState[position] = enabled;
        this.clickable[position] = clickable;
        //notifyDataSetInvalidated();

    }

    public int getCount()
    {
        // return the number of records in cursor

        if (itemsWithGroups == null)
            return 0;
        else
            return itemsWithGroups.size();
    }

    /**
     * Looks for a specific Group and mainfield item and returns the position
     * @param grpField - Grpfield to look for
     * @param mainField - text within the group to look for
     * @return
     */
    public int getItem (String grpField, String mainField) {

        //scan the ArrayList of itemsWithGroups looking for the grpField.
        //Once found continue looking for the mainField and return thatindex
        boolean grpFound = false;
        int pos = 0;

        // scan itemsWithGroup array looking for group field in group hrader
        while (pos < itemsWithGroups.size() && !grpFound) {

            if (itemsWithGroups.get(pos).rowIsGroupHeader && itemsWithGroups.get(pos).rowText.equals(grpField))
                grpFound = true;
            else
                pos++;
        }

        //start at next row in ArrayList
        pos++;

        //if group was found, continue searching until we run out of rows, or we come across another group header
        while (grpFound && pos < itemsWithGroups.size() && !itemsWithGroups.get(pos).rowIsGroupHeader) {

            if (itemsWithGroups.get(pos).rowText.equals(mainField))
                return pos;
            else
                pos++;
        }

        return ITEM_NOT_FOUND;
    }

    /**
     * getItem - looks in the MainField for a specific value and returns the position in the listbox
     * @param mainField - string to find
     * @return returned position, or ITEM_NOT_FOUND
     */
    public int getItem(String mainField)
    {

        if (items == null)
            return ITEM_NOT_FOUND;

        //first scan the array looking for MainField value that is passed
        for (int p=0; p < items.length; p++)

            // if found.....
            if (items[p].equals(mainField))

                //then scan the itemsWithGroup ArrayList looking items index, and return row position
                for (int i = 0; i < itemsWithGroups.size() ; i++)
                    if (itemsWithGroups.get(i).arraysIndex == p)
                        return i;

        return ITEM_NOT_FOUND;
    }

    /**
     * getItem - looks in the passed generic field for a specific value and returns the position in the listbox
     * @param textToLookFor - string to find
     * @param genericCodeField - enumerated GroupBy to look for
     * @return returned position, or ITEM_NOT_FOUND
     */
    public int getItem(String textToLookFor, GroupBy genericCodeField)
    {

        String[] itemsGeneric = codes[genericCodeField.ordinal()];

        if (itemsGeneric == null)
            return ITEM_NOT_FOUND;

        //first scan the array looking for generic value that is passed
        for (int p=0; p < itemsGeneric.length; p++)

            // if found.....
            if (itemsGeneric[p].equals(textToLookFor))

                //then scan the itemsWithGroup ArrayList looking items index, and return row position
                for (int i = 0; i < itemsWithGroups.size() ; i++)
                    if (itemsWithGroups.get(i).arraysIndex == p)
                        return i;

        return ITEM_NOT_FOUND;
    }

    /**
     * getItem - looks in two generic field for specific values and returns the position in the listbox.  Both field must match
     * @param textToLookFor1 - first string to find
     * @param genericCodeField1 - first enumerated GroupBy to look for textToLookFor1
     * @param textToLookFor2 - Second string to find
     * @param genericCodeField2 - Seconf enumerated GroupBy to look for textToLookFor1
     * @return returned position, or ITEM_NOT_FOUND
     */
    public int getItem(String textToLookFor1, GroupBy genericCodeField1, String textToLookFor2, GroupBy genericCodeField2)
    {

        String[] itemsGeneric1 = codes[genericCodeField1.ordinal()];
        String[] itemsGeneric2 = codes[genericCodeField2.ordinal()];

        if (itemsGeneric1 == null)
            return ITEM_NOT_FOUND;

        if (itemsGeneric2 == null)
            return ITEM_NOT_FOUND;

        //first scan the first array looking for generic value 1 and generic value 2 that is passed
        for (int p=0; p < itemsGeneric1.length; p++)

            // if found.....
            if (itemsGeneric1[p].equals(textToLookFor1) && itemsGeneric2[p].equals(textToLookFor2))

                //then scan the itemsWithGroup ArrayList looking items index, and return row position
                for (int i = 0; i < itemsWithGroups.size() ; i++)
                    if (itemsWithGroups.get(i).arraysIndex == p)
                        return i;

        return ITEM_NOT_FOUND;
    }

    /**
     * Pass the otiginal array index of items[], and get a return of the displayable group.
     * @param indexToFind
     * @return
     */
    public int getPositionFromArrayIndex(int indexToFind) {

        for (int i = 0; i < itemsWithGroups.size() ; i++) {
            if (itemsWithGroups.get(i).arraysIndex == indexToFind)
                return i;
        }

        return ITEM_NOT_FOUND;
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    //Return whether the row at a position is a Group.  true is yes.
    public boolean isGroup(int position) {

        //if position passed is less than zero, assume if a group row
        if (position < 0)
            return true;
        else
            return itemsWithGroups.get(position).rowIsGroupHeader;
    }

    //This gets called prior to each row getting added in listview.  Return false to disable.
    @Override
    public boolean isEnabled(int position) {
        // TODO Auto-generated method stub
        if (!clickable[position]) {
            return false;
        }

        return true;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);

    }

    public View getCustomView(final int position, View convertView, final ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Load the correct group, whether its a header,  group or regular list row
        convertView = inflater.inflate(itemsWithGroups.get(position).lstRowLayout, parent, false);
/*
        if (itemsWithGroups.get(position).rowIsListViewHeader)
            convertView = inflater.inflate(itemsWithGroups.get(position).lstRowLayout, parent, false);
        else
            if (itemsWithGroups.get(position).rowIsGroupHeader)
                convertView = inflater.inflate(itemsWithGroups.get(position).lstRowLayout, parent,  false);
            else
                convertView = inflater.inflate(itemsWithGroups.get(position).lstRowLayout, parent, false);
                //convertView = inflater.inflate(this.lstRowlayout, parent, false);

*/

        //Get Linear Layout
        LinearLayout LL = (LinearLayout) convertView.findViewById(R.id.list_row_layout);

        // Main field value.  rowText will either have a group header or the row data
        final TextView txtMainField=(TextView)convertView.findViewById(R.id.txtMainField);
        txtMainField.setText(itemsWithGroups.get(position).rowText);

        //If this is not a group, then set the gravity that was passed.  If it is a group, assume centered
        if (!isGroup(position))
            //if Gravity was set, use it, otherwise default to what is in the XML layout for the field
            if (intMainFieldGravity != Gravity.NO_GRAVITY)
                txtMainField.setGravity(intMainFieldGravity);

        //If this is not a group, then set the minHeight if passed.  If it is a group, assume was it in layout
        if (!isGroup(position)) {

            //if MinHeight was set, use it, otherwise default to what is in the XML layout for the field
            if (intMainFieldMinHeight != MIN_HEIGHT_NO_VALUE) {
                txtMainField.setMinHeight(intMainFieldMinHeight);
                //LL.setMinimumHeight(intMainFieldMinHeight);

            }

            //if textsize was passed, use it.
            if (mainFieldTextSize != TEXT_SIZE_NO_VALUE)
                txtMainField.setTextSize(TypedValue.COMPLEX_UNIT_PX, mainFieldTextSize);
        }

        //txtMainField may be a checkbox, so set listener to store
        if (txtMainField instanceof CheckBox) {

            final View cv = convertView;
            ((CheckBox) txtMainField).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    setLastClickedViewID(txtMainField.getId());
                    checked.put(position, isChecked);

                    //If listener was set for onCheck, call it.
                    if (mOnViewListener != null)
                        mOnViewListener.onClick(position, cv, txtMainField);
                }
            });


            //Check the box on display.  Only do if the checkbox is not clickable.  If it is, that
            //means the only way to check it will be through the user clicking,
            if (!((CheckBox) txtMainField).isClickable())
                if (checked.get(position))
                    ((CheckBox) txtMainField).setChecked(true);
                else
                    ((CheckBox) txtMainField).setChecked(false);


        }

        /*
        //Handle the possibility of alternate fields in the layout.
        if (itemsWithGroups.get(position).alternateText1 != null) {
            TextView txtA1 = (TextView) convertView.findViewById(R.id.alternateText1);
            txtA1.setText(itemsWithGroups.get(position).alternateText1);
        }

        if (itemsWithGroups.get(position).alternateText2 != null) {
            TextView txtA2 = (TextView) convertView.findViewById(R.id.alternateText2);
            txtA2.setText(itemsWithGroups.get(position).alternateText2);
        }

        if (itemsWithGroups.get(position).alternateText3 != null) {
            TextView txtA3 = (TextView) convertView.findViewById(R.id.alternateText3);
            txtA3.setText(itemsWithGroups.get(position).alternateText3);
        }
        */

        /*
        //Look to see if a pinfall view is present
        BowlingPinsView bpv = (BowlingPinsView) convertView.findViewById(R.id.BowlingPinView);
        if (bpv != null) {
            bpv.setPinsDown(1,1, new boolean[]  {true, true, true, true, true, true, true, true, true, false});
            bpv.setPinsDown(1,2, new boolean[]  {true, true, true, true, true, true, true, true, true, true});
            bpv.showPins(true, 1, 1);
        }
        */

        //Non group row values.
        if (!itemsWithGroups.get(position).rowIsGroupHeader) {

            // get the reference of textViews
            TextView txtCodeG1 = (TextView) convertView.findViewById(R.id.txtCodeG1);
            TextView txtCodeG2 = (TextView) convertView.findViewById(R.id.txtCodeG2);
            TextView txtCodeG3 = (TextView) convertView.findViewById(R.id.txtCodeG3);
            TextView txtCodeG4 = (TextView) convertView.findViewById(R.id.txtCodeG4);
            TextView txtCodeG5 = (TextView) convertView.findViewById(R.id.txtCodeG5);

            //get array index from current row
            int ai = itemsWithGroups.get(position).arraysIndex;

            //Now set the codes if they were passed
            if (txtCodeG1 !=null && codes[0] != null && codes[0].length > ai)
                txtCodeG1.setText(codes[0][ai]);

            if (txtCodeG2 !=null && codes[1] != null && codes[1].length > ai)
                txtCodeG2.setText(codes[1][ai]);

            if (txtCodeG3 !=null && codes[2] != null && codes[2].length > ai)
                txtCodeG3.setText(codes[2][ai]);

            if (txtCodeG4 !=null && codes[3] != null && codes[3].length > ai)
                txtCodeG4.setText(codes[3][ai]);

            if (txtCodeG5 !=null && codes[4] != null && codes[4].length > ai)
                txtCodeG5.setText(codes[4][ai]);

            if (codesUsed == CodesUsed.Ten) {

                TextView txtCodeG6 = (TextView) convertView.findViewById(R.id.txtCodeG6);
                TextView txtCodeG7 = (TextView) convertView.findViewById(R.id.txtCodeG7);
                TextView txtCodeG8 = (TextView) convertView.findViewById(R.id.txtCodeG8);
                TextView txtCodeG9 = (TextView) convertView.findViewById(R.id.txtCodeG9);
                TextView txtCodeG10 = (TextView) convertView.findViewById(R.id.txtCodeG10);

                if (txtCodeG6 !=null && codes[5] != null && codes[5].length > ai)
                    txtCodeG6.setText(codes[5][ai]);

                if (txtCodeG7 !=null && codes[6] != null && codes[6].length > ai)
                    txtCodeG7.setText(codes[6][ai]);

                if (txtCodeG8 !=null && codes[7] != null && codes[7].length > ai)
                    txtCodeG8.setText(codes[7][ai]);

                if (txtCodeG9 !=null && codes[8] != null && codes[8].length > ai)
                    txtCodeG9.setText(codes[8][ai]);

                if (txtCodeG10 !=null && codes[9] != null && codes[9].length > ai)
                    txtCodeG10.setText(codes[9][ai]);

            }


            //disable row so it uses the correct selector colors.
            if (!positionState[position]) {
                txtMainField.setEnabled(false);
                //convertView.setEnabled(false);
            } else {
                txtMainField.setEnabled(true);
                convertView.setEnabled(true);

            }

        }
        else {

            //if grouprow is a checkbox, make enabled and clickable, else make enabled, but not clickable
            if (txtMainField instanceof CheckBox)
                setPositionState(position, true, true);
            else
                setPositionState(position, true, false);

        }

        return convertView;
    }


    }

