package evets.neg.ballspeed;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Steve on 5/21/2015.
 *
 * DBHelperTransaction is used to support transaction processing of SQLLites add, deletes, updated
 * To use, do something like this:

 //Get an instance of DBHelperTransaction class
 DBHelperTransaction dbt =  DBHelperTransaction.getInstance(getActivity());

 //Pass the helper class onto the class with actual SQL inserts, updates, deletes.  This class needs to be
 //setup to use transaction processing.  See DB_BolwerTable.java
 DB_BowlerTable sqlDB = new DB_BowlerTable(dbt);

 //Start transaction
 dbt.beginTransactionDB();

 //perform inserts, updates, deletes
 sqlDB.InsertBallRecord("Neglia", "Steve","SNEG","M","R");
 sqlDB.InsertBallRecord("Neglia", "Steve","SNEG","M","L");
 sqlDB.InsertBallRecord("Neglia", "Mary Abb","MAR","F","L");

 //commit the transaction
 dbt.commitTransactioDB();



 *
 */
public class DBHelperTransaction extends DBHelperBase {

    private static DBHelperTransaction mInstance;

    //SQLLite transaction DB for when individual classes need to work in transactions
    private SQLiteDatabase sqlTransDB;

    //Pass context to base class.  purposely private so class can not be instantiated.  Call getINstance instead.
    private DBHelperTransaction(Context context) {

        super(context);
    }

    //Call this method to return an instance of DBHelperTransaction.  This ensure only once instance will
    //ever be created.
    public static DBHelperTransaction getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DBHelperTransaction(context);
        }
        return mInstance;
    }


    /**
     * Are we in transaction mode
     * @return true since this class if for transactionmode processing
     */
    public boolean inTransactionMode() {
        return true;
    }


    /**
     * This is called when we need to start transaction processing, and not a single update, insert or delete
     * call this method to return an instance of a transaction DB.  It should be followed up with
     * commitTransactionDB when completed
     * @return SQLLiteDatase in transaction mode
     */
    public SQLiteDatabase beginTransactionDB() {

        sqlTransDB = getWritableDatabase();
        sqlTransDB.beginTransaction();

        return sqlTransDB;
    }

    /**
     * This method should only be called after beginTransactionDB is called to commit the transaction
     */
    public void commitTransactioDB() {

        //set to successful to commit
        sqlTransDB.setTransactionSuccessful();

        //and end....
        sqlTransDB.endTransaction();

        sqlTransDB.close();
    }

    /**
     * This method should only be called if we do not want a transaction committed successfully.
     */
    public void rollBackTransactionDB() {

        //and end....
        sqlTransDB.endTransaction();
        sqlTransDB.close();

    }

    /**
     * return a the transaction SQLLite DB for writing.  Since we're in transaction mode, it will return the already opened database
     * This assume BeginTrabsaction was called first.
     * @return SQLiteDatabase
     */
    public SQLiteDatabase getSQLDB() {

            return sqlTransDB;

    }

    /**
     * Do nothing.  Calling client will call commitTransaction when its ready to commit and close the database
     * This is only here because some methods may call close in single transaction mode and we need
     * to catch the call
     */
    public void closeDB() {

        //do nothing since we do not want a transaction to close the db
        //this function is here so the parent does not get called.

    }


}
