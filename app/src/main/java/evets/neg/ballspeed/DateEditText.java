package evets.neg.ballspeed;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by Steve on 9/3/2015.
 */
public class DateEditText extends LinearLayout {

    public static final int DATE_VALUE_INVALID = -1;

    private EditText mm;
    private EditText dd;
    private EditText yyyy;
    private ImageView cal;

    int MM,DD, YYYY;

    private Context context;

    CalendarViewDialogFragment calViewDialogFragment;

    @Override
    public Parcelable onSaveInstanceState() {

        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        bundle.putInt("MM", getMonth());
        bundle.putInt("DD", getDay());
        bundle.putInt("YYYY", getYear());
        return bundle;
    }


    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        if (state instanceof Bundle) // implicit null check
        {
            Bundle bundle = (Bundle) state;
            MM = bundle.getInt("MM");
            DD = bundle.getInt("DD");
            YYYY = bundle.getInt("YYYY");
            state = bundle.getParcelable("superState");
        }
        super.onRestoreInstanceState(state);
    }

    /**
     * Call this by client after a rotate
     */
    public void refreshViewAfterRotate() {

        setMonth(MM);
        setDay(DD);
        setYear(YYYY);
    }


    public DateEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.date_text_field, this);

        this.context = context;
        setupViewItems();

        //Look to see if the TimeViewDialogFragment was opened.  If so, reset the listener.
        calViewDialogFragment = (CalendarViewDialogFragment) scanForActivity(context).getFragmentManager().findFragmentByTag("Calendar");
        if (calViewDialogFragment != null)
            calViewDialogFragment.setOnDismissListener(calListener);

    }


    private CalendarViewDialogFragment.DialogDismissListener calListener = new CalendarViewDialogFragment.DialogDismissListener() {

        @Override
        public void OnSetClicked(int year, int month, int dayOfMonth) {

            //build returned date.  Add 1 to month since calendar is zero based.
            String date = Utils.formatDate(year, month + 1, dayOfMonth);
            setDate(date);
            calViewDialogFragment.dismiss();

        }

        @Override
        public void OnCancelClicked() {
            calViewDialogFragment.dismiss();
        }
    };

        private void setupViewItems() {

        mm = (EditText) findViewById(R.id.edtMM);
        dd = (EditText) findViewById(R.id.edtDD);
        yyyy = (EditText) findViewById(R.id.edtYYYY);
        cal = (ImageView) findViewById(R.id.imgCalendar);


        //if the length entered in day is 2, move onto year field
        mm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

               if (mm.getText().toString().length() == 2)
                    dd.requestFocus();
            }
        });

        //if the length entered in day is 2, move onto year field
        dd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (dd.getText().toString().length() == 2)
                    yyyy.requestFocus();
            }
        });


        //open calendar for for DateBowledOn
        cal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

              Activity act = scanForActivity(context);
                FragmentManager manager = act.getFragmentManager();

                  //FragmentManager manager = ((Activity) context).getFragmentManager();
                calViewDialogFragment = new CalendarViewDialogFragment();
                calViewDialogFragment.setOnDismissListener(calListener);

                String date = getDate();
                if (date != null)
                    calViewDialogFragment.setDate(date);

                //show the dialog
                calViewDialogFragment.show(manager, "Calendar");

            }
        });
    }

    /**
     * Set month field with value passed
     *
     * @param m month to set
     */
    public void setMonth(int m) {

        if (m>0)
            mm.setText(String.format("%02d", m));
        else
            mm.setText("");

    }

    /**
     * Set day field with value passed
     *
     * @param d month to set
     */
    public void setDay(int d) {

        if (d>0)
            dd.setText(String.format("%02d", d));
        else
            dd.setText("");

    }

    /**
     * Set year field with value passed
     *
     * @param y month to set
     */
    public void setYear(int y) {

        if (y>0)
            yyyy.setText(String.format("%02d", y));
        else
            yyyy.setText("");

    }

    /**
     * Accept date in format of yyyy-mm-dd and if valid, format the date month, day, year field
     *
     * @param date - date as yyyy-mm-dd
     * @return true if successful, false if otherwise
     */
    public boolean setDate(String date) {

        if (Utils.validateOrConvertDate(date) != null) {

            setMonth(Utils.parseDate(date, Utils.DatePart.MM));
            setDay(Utils.parseDate(date, Utils.DatePart.DD));
            setYear(Utils.parseDate(date, Utils.DatePart.YYYY));

            clearFocus();

            return true;
        } else
            return false;
    }

    /**
     * Return month field
     *
     * @return month value, or DATE_VALUE_INVALID
     */
    public int getMonth() {

        if (mm.getText().length() > 0)
            return Integer.parseInt(mm.getText().toString());
        else
            return DATE_VALUE_INVALID;
    }

    /**
     * Return day field
     *
     * @return day value, or DATE_VALUE_INVALID
     */
    public int getDay() {

        if (dd.getText().length() > 0)
            return Integer.parseInt(dd.getText().toString());
        else
            return DATE_VALUE_INVALID;
    }

    /**
     * Return yer field
     *
     * @return year value, or DATE_VALUE_INVALID
     */
    public int getYear() {

        if (yyyy.getText().length() > 0)
            return Integer.parseInt(yyyy.getText().toString());
        else
            return DATE_VALUE_INVALID;
    }

    /**
     * Return a date formed as yyyy-mm-dd, but onlyif all parts entered are valid.

     * @returns the date, or null if its not valid
     */
    public String getDate() {

        if (getDay() != DATE_VALUE_INVALID && getMonth() != DATE_VALUE_INVALID && getYear() != DATE_VALUE_INVALID)
            return Utils.validateOrConvertDate(Utils.formatDate(getYear(), getMonth(), getDay()));
        else
            return null;
    }

    /**
     * Method return true if date was started, but is invalid
     * @return
     */
    public boolean isDateStartedAndInvalid() {

        if ((dd.getText().length() > 0 || mm.getText().length() > 0 || yyyy.getText().length() > 0) && getDate() == null)
            return true;
        else
            return false;

    }

    /**
     * SHow or hide the calendar
     * @param visibility View.VISIBLE, VIEW.INVISIBLE, View.GONE
     */
    public void setCalendarVisibility(int visibility) {

            cal.setVisibility(visibility);
    }

    /**
     * Disable all the fields and hide calendar
     * @param val
     */
    @Override
    public void setEnabled(boolean val) {
        mm.setEnabled(val);
        dd.setEnabled(val);
        yyyy.setEnabled(val);

        setFocusable(val);

        if (val)
            cal.setVisibility(VISIBLE);
        else {
            cal.setVisibility(INVISIBLE);
            clearFocus();
        }
        super.setEnabled(val);
    }



    /**
     * Default the fields to the current date
     */
    public void setDateAsCurrent() {

        setDate(Utils.getCurrentDate());
    }

    /**
     * blank out all fields
     */
    public void clearDateFields() {
        mm.setText("");
        dd.setText("");
        yyyy.setText("");
    }

    public void clearFocus() {

        mm.clearFocus();
        dd.clearFocus();
        yyyy.clearFocus();
    }

    public void setFocusable(boolean val) {
        mm.setFocusable(val);
        dd.setFocusable(val);
        yyyy.setFocusable(val);
        mm.setFocusableInTouchMode(val);
        dd.setFocusableInTouchMode(val);
        yyyy.setFocusableInTouchMode(val);
    }

    /**
     * Determine activity of the context
     * @param cont
     * @return
     */
    private static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity)cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper)cont).getBaseContext());

        return null;
    }

}
