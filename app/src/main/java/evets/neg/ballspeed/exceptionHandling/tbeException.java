package evets.neg.ballspeed.exceptionHandling;

/**
 * Created by Steve on 5/23/2015.
 */
public class tbeException extends Exception {

    private String retMessage;
    private Throwable e;

    public tbeException(Exception e) {
        super(e);
        this.e = e;
        formatError();
    }

    public tbeException(String errorMsg) {
        super();

    }

    private void formatError() {

        retMessage = "A fatal error has occurred: \n";
        retMessage += e.toString() + "\n\nStacktrace:\n\n ";

        StackTraceElement[] ste = e.getStackTrace();

        for (int i=0 ; i<ste.length; i++) {
            retMessage += ste[i].toString() + "\n";
        }
    }

    public String getErrorMessage() {return retMessage;}
}
