package evets.neg.ballspeed;

/**
 * Created by Steve on 6/2/2015.
 */
public class AppInfo {


    //App database version.  Update this when a new version of DB is distributed and is represented in
    //asset file db_sql.xml
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "ballspeed.db";
    public static final String DB_NAME_BACKUP = "ballspeed_backup.db";
}
