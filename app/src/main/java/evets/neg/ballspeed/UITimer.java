package evets.neg.ballspeed;

import android.app.Activity;
import android.view.View;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Steve on 7/19/2015.
 *
 * Object is built to initiate a timer on the user interface thread, allowing for updates to the user interface.
 * To use:
 *
 *  initiate object, constructor, UITimer(Activity uiThread, OnUpdateTimer activityCallback), passing the Activity and a a new UITimer.OnUpdateTimer()
 *  As an alterbnate a constructor is available that does not pass a new UITimerOnUpdateTimer.  This would mean there would be no direct communication back to the Activity for updates
 *  Call startTimer and pass milliseconds for interval updates.  startTimer will call the calling activities updateTimer implementation
 *  Call stopTimer to stop the timer
 *  Call isTimerRunning to determine if the timer is running
 *  Call getTimerValues for a returned class of TimerValues including milliseconds, seconds, minutes and hours
 *
 *  Every x milliseconds passed into startTimer, the calling activities UpdateTimer declared method will be called.  Total elapased hours, minutes, seconds and milliseconds
 *  will be returned.
 *
 *  As an example of use,:


 /Initiate a UI Timer that will return back to this Activity
 timer = new UITimer((Activity) context, new UITimer.OnUpdateTimer() {

 //this will be called by the UITimer object to process the data.
 public void UpdateTimer(long hour, long minute, long seconds, long milliseconds) {
 txtChron.setText((hour + "." + minute + "." + seconds + "." + milliseconds));
 }
 });

 //Set Listeners for start/stop button
 butStartStop.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

//start the timer.  have it return back to the activity every 50 milliseconds for updating
if (!timer.isTimerRunning()) {
butStartStop.setText("Stop");
timer.startTimer(50);
}
else {
timer.stopTimer();
butStartStop.setText("Start");
}
}
});
 */


public class UITimer {

    private long startTime;
    private boolean isTimerRunning;
    private Timer timer;
    private Activity uiThread;
    private View view;
    private long sec;
    private long mill;
    private long hrs;
    private long min;

    //publically returnable class of values
    public class TimerValues {
        public long milliseconds;
        public long seconds;
        public long minutes;
        public long hours;
    }

    OnUpdateTimer activityCallback;

    public interface OnUpdateTimer
    {
        public void UpdateTimer(long hour, long minute, long seconds, long milliseconds);
    }

    /**
     * Alternate Constructor to initialize Timer.  No Activity calback is passed, so nothing will be sent back to Activiy
     * @param uiThread - thread the timer is to communicate to
     */

    public UITimer(Activity uiThread) {

        this.uiThread = uiThread;
        isTimerRunning = false;
        this.activityCallback = null;
    }

    /**
     * Set listener to be called by this service so activity can handle the event when the timer task initiates.
     * @param activityCallback
     */
    public void setOnUpdateTimer(OnUpdateTimer activityCallback) {

        //activity callback was passed
        this.activityCallback = activityCallback;

    }


    /**
    * return boolean value if time is running
    **/
    public boolean isTimerRunning() {
        return this.isTimerRunning;
    }

    /**
     * Start the timer
     * @param milliIntervals -  how often to execute the Timer Task that will communicate back to the Activity
     */
    public void startTimer(long milliIntervals) {

        timer = new Timer();
        startTime = 0;
        sec = 0;
        mill = 0;
        hrs = 0;
        min = 0;
        isTimerRunning = true;
        MyTimerTask myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 0, milliIntervals);

    }

    /*
    ** Cancel the timer that is running.
     */
    public void stopTimer() {

        timer.cancel();
        timer.purge();
        timer = null;
        isTimerRunning = false;

    }

    public TimerValues getTimerValues() {

        TimerValues tv = new TimerValues();
        tv.seconds = sec;
        tv.hours = hrs;
        tv.milliseconds = mill;
        tv.minutes = min;

        return tv;

    }

    /**
     * Timer task that is executed
     */
    private class MyTimerTask extends TimerTask {


        @Override
        public void run() {

            Date date = new Date();
            final long msDate = date.getTime();

            //record starttime to get elapsed time
            if (startTime == 0)
                startTime = msDate;

            uiThread.runOnUiThread(new Runnable(){

                @Override
                public void run() {

                    if (timer == null)
                        cancel();

                    long dts = msDate - startTime;

                    if (dts >= 1000) {
                        hrs = (dts/(1000*60*60)) % 24;
                        min = (dts / (1000*60)) % 60;
                        sec = dts / 1000 % 60;
                        mill = dts % 1000;
                    }
                    else
                        mill = dts;

                    if (activityCallback != null)
                        activityCallback.UpdateTimer(hrs,min,sec,mill);

                }});
        }

    }

}
