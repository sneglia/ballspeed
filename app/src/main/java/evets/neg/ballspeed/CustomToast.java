package evets.neg.ballspeed;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by Steve on 4/5/2015.
 */
public class CustomToast {

    private int duration = Toast.LENGTH_SHORT;
    private int gravity = Gravity.CENTER_VERTICAL;
    private int xOffset = 0;
    private int yOffset = 0;

    public static int CUSTOM_TOAST_GRAVITY = 100;

    public void toast(Context context, int xmlRLayout, int viewID, String textToDisplay) {

        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the Layout
        View layout = inflater.inflate(xmlRLayout, null);

        TextView text = (TextView) layout.findViewById(viewID);

        // Set the Text to show in TextView
        text.setText(textToDisplay);

        Toast toast = new Toast(context);
        toast.setGravity(gravity, xOffset, yOffset);
        toast.setDuration(duration);
        toast.setView(layout);
        toast.show();
    }

    public void setDuration(int X) {
            duration=X;
    }

    public void setGravity(int gravity) {
        this.gravity=gravity;
    }

    public void setGravity(int gravity, int xOffset, int yOffset) {

        this.gravity=gravity;
        this.xOffset = xOffset;
        this.yOffset = yOffset;

    }

}
