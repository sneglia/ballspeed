package evets.neg.ballspeed;

import android.app.Activity;

/**
 * Created by Steve on 7/19/2015.
 */
public class BallSpeed extends UITimer {

    private int distance;

    private static final int MODE_MTRS_PER_HOUR = 1;
    private static final int MODE_MILES_PER_HOUR = 2;
    private static final int MODE_KILOS_PER_HOUR = 3;

    /**
     * Alternate constructor.  If used, no method will be called in Activity.  After the timer is done, user will
     * have to manually call to get values.
     * @param uiThread
     * @param distance
     */
    public BallSpeed(Activity uiThread, int distance) {

        super(uiThread);

        //save distance for calculation Test
        this.distance = distance;

    }

    /**
     * pass distance in for calculation
     * @param distance
     */
    public void setDistance(int distance) {
        this.distance = distance;
    }

    /**
     * get distance
     * @return  distance
     */
    public int getDistance() {
        return this.distance;
    }

    public float getBallSpeedMilesPerHour() {

        return computeBallSpeed(MODE_MILES_PER_HOUR);
    }

    /**
     *
     * @return ball speed as meters per second
     */
    public float getBallSpeedKilometersPerHour() {

        return computeBallSpeed(MODE_KILOS_PER_HOUR);
    }

    /**
     *
     * @return ball speed as meters per second
     */
    public float getBallSpeedMetersPerSecond() {

        return computeBallSpeed(MODE_MTRS_PER_HOUR);
    }

    /**
     * This method uses the simple physics formula Speed = distance/time to compute ball speed.
     * Some notes, assuming 60 feet:
     *
     * 1 MTR = 3.28034 FT
     * 60 FT = 18.288 MTRS
     * Foul Line to Head Pin = 60 Feet
     * 1 MTR/SEC = 2.23694 Miles/Hour
     * speed = distance/time, or s=d/t in mtrs/sec
     * Speed = 18.2888/ball speed in seconds, returns mtr/sec
     * Speed = speed * 2.23694 converts to miles per hour
     *
     * @param mode - pas MODE_MILES_PER_HOUR or MODE_METERS_PER_HOUR depending on desired return
     * @return - computed value
     */
    private float computeBallSpeed(int mode) {

        double ballSpeedMetersPerSecond = 0;
        double ballSpeedMilesPerHour = 0;
        double ballSpeedKiloPerHour = 0;
        double seconds;

        //Obtain the raw values
        UITimer.TimerValues tv = super.getTimerValues();
        seconds = (float) tv.seconds + (float) tv.milliseconds/1000;

        //compute Ball Speed at meters per second
        final double MtrPerFoot = 3.28034;
        double distInMtr = distance / MtrPerFoot;
        ballSpeedMetersPerSecond = distInMtr /seconds;

        //convert Ball speed to miles per hour
        final double mtrPerSecToMPH = 2.23694;
        ballSpeedMilesPerHour = ballSpeedMetersPerSecond * mtrPerSecToMPH;

        final double mtrPerSectoKPH = 3.6;
        ballSpeedKiloPerHour = ballSpeedMetersPerSecond * mtrPerSectoKPH;

        switch (mode) {
            case MODE_MTRS_PER_HOUR:
                return (float) ballSpeedMetersPerSecond;
            case MODE_KILOS_PER_HOUR:
                return (float) ballSpeedKiloPerHour;
            case MODE_MILES_PER_HOUR:
                default:
                return (float) ballSpeedMilesPerHour;
        }
    }
}
