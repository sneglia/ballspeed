package evets.neg.ballspeed;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

/**
 * Created by Steve on 8/15/2015.
 */
public class CalendarViewDialogFragment extends DialogFragment {

    //anybod using this dialogfragment should declare this interface id it wants to know upon return.
    public interface DialogDismissListener {
        public void OnSetClicked(int year, int month, int dayOfMonth);
        public void OnCancelClicked();
    }

    //return values
    private int retYear, retMonth, retDayOfMonth;

    private DialogDismissListener mDismissListener;

    //field windows
    private MaterialCalendarView calView;
    private Button butOK;
    private Button butCancel;

    //Constructor
    public CalendarViewDialogFragment() {
        mDismissListener = null;

        //Assume current date.
        retYear = Utils.getCurrentDateParts(Utils.DatePart.YYYY);
        retMonth = Utils.getCurrentDateParts(Utils.DatePart.MM);
        retDayOfMonth = Utils.getCurrentDateParts(Utils.DatePart.DD);;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_calendar_view, null, false);

        //create references to fields
        butOK = (Button) view.findViewById(R.id.butOK);
        butCancel = (Button) view.findViewById(R.id.butCancel);
        calView = (MaterialCalendarView) view.findViewById(R.id.calendarID);

        //no title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        calView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                retYear = date.getYear();
                retMonth = date.getMonth();
                retDayOfMonth = date.getDay();

            }
        });


        //calback to the calling routine if OK is clicked
        butOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mDismissListener != null) {
                    mDismissListener.OnSetClicked(retYear, retMonth, retDayOfMonth);
                }
            }
        });

        //calback to the calling routine if OK is clicked
        butCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mDismissListener != null) {
                    mDismissListener.OnCancelClicked();
                }
            }
        });

        //set the date on the calendar
        CalendarDay cal = new CalendarDay(retYear, retMonth, retDayOfMonth);
        calView.setSelectedDate(cal);
        calView.state().edit().setCalendarDisplayMode(CalendarMode.MONTHS).commit();


        //Do not allow rotate when showing calendar so we don't lose this as a top dialogfragment
        Utils.lockScreenOrientation(getActivity(), true);
        return view;
    }



    //hold on to listener for when a button is pressed
    public void setOnDismissListener(DialogDismissListener listener) {
        mDismissListener = listener;
    }

    /**
     * method accepts date as a string as normal.  Month SHOULD NOT BE ZERO BASED.
     * @param date
     */
    public void setDate(String date) {

        retYear = Utils.parseDate(date, Utils.DatePart.YYYY);
        retMonth = Utils.parseDate(date, Utils.DatePart.MM)-1;
        retDayOfMonth = Utils.parseDate(date, Utils.DatePart.DD);

    }

    public void onResume() {

        super.onResume();
    }

    public void onDestroy() {

        super.onDestroy();
        Utils.lockScreenOrientation(getActivity(), false);

    }
}
