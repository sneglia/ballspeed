package evets.neg.ballspeed;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Steve on 6/17/2015.
 *
 *
 * Common FUnction Created:
 *
 *
 *
 */
public class DB_HistoryTable {

    //Table Name
    public static String TABLE_NAME = "History";

    //BowlingAlley Table Column Name
    public static String COL_ID = "ID_Bowler";
    public static String COL_TIMESTAMP = "TimeStamp";
    public static String COL_TARGET_SPEED= "TargetSpeed";
    public static String COL_DEVIATION= "Deviation";
    public static String COL_SECONDS= "Seconds";
    public static String COL_MPH= "MPH";
    public static String COL_KPH= "KPH";
    public static String COL_IS_HIT= "IsHit";

    public static String IS_HIT_YES = "Y";
    public static String IS_HIT_NO = "N";
   //enumerated sort order options that can be passed in
    public enum sortOrder {
        ASCENDING,
        DESCENDING,
        RANDOM
    }

    //holds sorder order;
    private sortOrder so;

    public enum showActiveInactive {
        ACTIVE,
        INACTIVE,
        BOTH
    }
    private showActiveInactive showAI;

    //DBHelper Variable
    private DBHelperBase db;

    //Constructor.  Pass the single dbHelper instance
    public DB_HistoryTable(DBHelperBase db) {

        this.db = db;

        //default sortorder and Active/Inactive
        so = sortOrder.ASCENDING;
        showAI = showActiveInactive.BOTH;

    }

    /**
     *    methods can be used by caller for all default sortOrder of queries
     */
    public void setSortOrder(sortOrder so) {
        this.so = so;
    }


    /**
     *    methods can be used by caller for all default Active/Inactive Stats
     */
    public void setDefaultActiveInactive(showActiveInactive showAI){
        this.showAI = showAI;
    }

    /**
    * getBowlerTableusingID()
      * @param bowlerID
    * Returns a List of HashMap objects of bowlers,
    * Calling function should cast to an ArrayList
    */
     public List getHistoryTableUsingID(int bowlerID, String startDate, String endDate, boolean all, boolean hits, boolean misses)  {

        int cnt = 0;
        List<Map<String, String>> lst = new ArrayList<Map<String, String>>();

        SQLiteDatabase sqlDB = null;
        sqlDB = db.getSQLDB();

        //Set Default dates
        if (startDate == null)
             startDate = "0000-00-00";

        if (endDate == null)
             endDate = "9999-12-31";

        String sqlStatement = "select * from " + TABLE_NAME + " where " + COL_ID + " = ? ";
        sqlStatement += " and " + COL_TIMESTAMP + " between ? and ? ";

         if (hits || misses) {

             if (hits && !misses)
                 sqlStatement += " and " + COL_IS_HIT + " = '" + IS_HIT_YES + "'";
             else if (!hits && misses)
                 sqlStatement += " and " + COL_IS_HIT + " = '" + IS_HIT_NO + "'";
             else if (hits && misses) {
                 sqlStatement += " and (" + COL_IS_HIT + " = '" + IS_HIT_YES + "'";
                 sqlStatement += "  or " + COL_IS_HIT + " = '" + IS_HIT_NO + "')";
             }
         }

         switch (so) {
             case DESCENDING:
                 sqlStatement += " order by " + COL_TIMESTAMP + " desc";
                 break;
             default:
                 sqlStatement += " order by " + COL_TIMESTAMP + " asc";
                 break;
         }

        String[] args = {bowlerID+"", startDate, endDate};
        Cursor c = sqlDB.rawQuery(sqlStatement, args);
        c.moveToFirst();

        cnt = c.getCount();

        if (cnt > 0)
        do {

            Map<String, String> map = new HashMap<String, String>();
            map.put(DB_HistoryTable.COL_ID, c.getString(c.getColumnIndex(DB_HistoryTable.COL_ID)));
            map.put(DB_HistoryTable.COL_TIMESTAMP, c.getString(c.getColumnIndex(DB_HistoryTable.COL_TIMESTAMP)));
            map.put(DB_HistoryTable.COL_TARGET_SPEED, c.getString(c.getColumnIndex(DB_HistoryTable.COL_TARGET_SPEED)));
            map.put(DB_HistoryTable.COL_DEVIATION, c.getString(c.getColumnIndex(DB_HistoryTable.COL_DEVIATION)));
            map.put(DB_HistoryTable.COL_IS_HIT, c.getString(c.getColumnIndex(DB_HistoryTable.COL_IS_HIT)));
            map.put(DB_HistoryTable.COL_KPH, c.getString(c.getColumnIndex(DB_HistoryTable.COL_KPH)));
            map.put(DB_HistoryTable.COL_MPH, c.getString(c.getColumnIndex(DB_HistoryTable.COL_MPH)));
            map.put(DB_HistoryTable.COL_SECONDS, c.getString(c.getColumnIndex(DB_HistoryTable.COL_SECONDS)));

            lst.add(map);

        } while (c.moveToNext());

        c.close();
        db.closeDB();

        return lst;


    }


    /**
     * insertHistoryRecord() - Insert a complete record into the History table
     * @param bowlerID
     * @param targetSpeed
     * @param deviation
     * @param seconds
     * @param mph
     * @param kph
     * @param isHit
     */
    public void insertHistoryRecord(int  bowlerID, float targetSpeed, float deviation, float seconds, float mph, float kph, String isHit) {


        //get DB, either normal or transaction
        SQLiteDatabase sqlDB = db.getSQLDB();

        ContentValues initialValues = new ContentValues();

        initialValues.put(COL_ID, bowlerID);

        if (targetSpeed == DBHelperBase.SET_AS_NULL_VALUE)
            initialValues.put(COL_TARGET_SPEED, (Byte) null);
        else
            initialValues.put(COL_TARGET_SPEED, targetSpeed);

        if (deviation == DBHelperBase.SET_AS_NULL_VALUE)
            initialValues.put(COL_DEVIATION, (Byte) null);
        else
            initialValues.put(COL_DEVIATION, deviation);

        initialValues.put(COL_SECONDS, seconds);
        initialValues.put(COL_MPH, mph);
        initialValues.put(COL_KPH, kph);
        initialValues.put(COL_IS_HIT, isHit);

        try {
            sqlDB.insertOrThrow(TABLE_NAME, null, initialValues);
        } catch(SQLException e) {
            MessageBox.ShowOKMessageBox(DBhelper.getContext(), "Error inserting History record: " + e.getMessage(), "Error");
        }

        //close the DB if not in transaction mode
        db.closeDB();

    }

    /**
     * Delete the history record for the passed _id and timestempa.
     * @param bowlerID Record ID for bowling alley.  Required
     * @param timeStamp Record ID for bowling alley.  Required
     */
public void deleteHistoryRecord(int bowlerID, String timeStamp) {

    String args[];

    //get DB, either normal or transaction
    SQLiteDatabase sqlDB = db.getSQLDB();

    ContentValues contentValues = new ContentValues();

    String whereClause = COL_ID + "= ? ";

    if (timeStamp != null) {
        whereClause += " and " + COL_TIMESTAMP + "= ?";
        args = new String[2];
        args[0] = bowlerID+"";
        args[1] = timeStamp;
    }
    else {
        args = new String[1];
        args[0] = bowlerID+"";
    }

    sqlDB.delete(TABLE_NAME, whereClause, args);

    //close the DB if not in transaction mode
    db.closeDB();

    }


}