package evets.neg.ballspeed;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.PopupMenu;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;


/**
 * Created by Steve on 6/10/2015.
 */
public class Utils {

    // Storage Permissions
    public static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    //Valid Phone Numbers
    private static final String PHONE_REGEX_V1 = "\\d{3}-\\d{4}"; //123-4567
    private static final String PHONE_REGEX_V2 = "\\d{3}-\\d{3}-\\d{4}"; //123-456-7890
    private static final String PHONE_REGEX_V3 = "[(]\\d{3}[)]-\\d{3}-\\d{4}"; //(123)-456-7890
    private static final String PHONE_REGEX_V4 = "[1]-\\d{3}-\\d{3}-\\d{4}"; //1-123-456-7890
    private static final String PHONE_REGEX_V5 = "[1]-[(]\\d{3}[)]-\\d{3}-\\d{4}"; //1-(123)-456-7890

    private static final String PHONE_REGEX_V1_CONVERT = "(\\d{3})(\\d{4})"; //1234567
    private static final String PHONE_REGEX_V2_CONVERT = "(\\d{3})(\\d{3})(\\d{4})"; //1234567789
    private static final String PHONE_REGEX_V3_CONVERT = "(\\d{1})(\\d{3})(\\d{3})(\\d{4})"; //11234567890
    private static final String PHONE_REGEX_V4_CONVERT = "(\\d{3})-(\\d{3})(\\d{4})"; //123-456-7789
    private static final String PHONE_REGEX_V5_CONVERT = "(\\d{1})-(\\d{3})-(\\d{3})(\\d{4})"; //1-123-4567890

    //Valid Date Value
    //private static final String DATE_REGEX_V1 = "\\d{4}-\\d{2}-\\d{2}"; //yyyy-mm-dd
    private static final String DATE_REGEX_V1 = "([19|20]\\d\\d)-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])"; //2015-12-31

    //Valid date values to check for conversion to something valid
    private static final String DATE_REGEX_V1_CONVERT = "((19|20)\\d\\d)[./-](0[1-9]|1[012]|[1-9])[/.-]([1-9]|0[1-9]|[12][0-9]|3[01])"; //handle yyyy/mm/dd with/without leading zeroes and allow periods.
    private static final String DATE_REGEX_V2_CONVERT = "(0[1-9]|1[012]|[1-9])[./-]([1-9]|0[1-9]|[12][0-9]|3[01])[./-]((19|20)\\d\\d)"; //handle mm/dd/yyyy with/without leading zeroes and allow periods.

    //Valid Time Value
    private static final String TIME_REGEX_V1 = "(0[1-9]|1[012]):(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]):(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])\\s(AM|PM)"; //12:10:45 AM


    /**
     * values to be passed for any method working in date or time parts such as parseDate and getCurrentDateParts
     */
    public static enum DatePart {YYYY, MM, DD, HH, MMM, SS, AMPM};


    /**
     * get App version name from build.gradle
     *
     * @param context application context
     * @return String representing version name
     */

    public static String getAppVersionName(Context context) {

        String v = null;
        try {
            v = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
        }

        return v;
    }

    /**
     * Raise the keyboard up
     *
     * @param c Activity context
     * @param v view to raise up for
     */
    public static void raiseKeyBoardForField(Context c, View v) {

        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);

    }

    /**
     * Simply returns the current year month or date
     * @param datePart pass in enum DatePart for what is to be returned, -1 if invalid DatePart
     */
    public static int getCurrentDateParts(DatePart datePart) {

        Calendar now = Calendar.getInstance();

        switch (datePart) {

            case YYYY:
                return now.get(Calendar.YEAR);
            case MM:
                return now.get(Calendar.MONTH);
            case DD:
                return now.get(Calendar.DAY_OF_MONTH);
        }

        return -1;
    }

    /**
     * Simply returns the current date/time in format "yyyy-MM-dd HH:mm:ss"
     */
    public static String getTimeStamp() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    /**
     * Simply returns the current date/time in format "yyyy-MM-dd"
     */
    public static String getCurrentDate() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    /**
     * Simply returns the current date/time in format "hh:mm:ss ampm"
     */
    public static String getCurrentTime() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    /**
     * Add passed number of days to a date formatted as yyyy-mm-dd
     * @param date
     * @param days
     * @return
     */
    public static String addDaysToDate(String date, int days)
    {
        Calendar aDate;

        Date myDate = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
             myDate = format.parse(date);
        }
            catch(Exception e) {}

            Calendar cal = Calendar.getInstance();
            cal.setTime(myDate);
            cal.add(Calendar.DATE, days); //minus number would decrement the days
            return format.format(cal.getTime());
    }

    public static enum DateCompares {Date1_Greater_Date2, Date2_Greater_Date1, Date1_Equal_Date2}

    /**
     * COmpare two dates for >,<, or equal
     * @param date1 - String date in format of yyyy-mm-dd
     * @param date2 String date in format of yyyy-mm-dd
     * @param dc, valie of DateCompares enum for what to do
     * @return true if requested check holds true;
     */
    public static boolean compareDates(String date1, String date2, DateCompares dc) {

        Date d1, d2;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");

        try {
            d1 = format.parse(date1);
            d2 = format.parse(date2);

            switch (dc) {

                case Date1_Greater_Date2:

                    if (d1.compareTo(d2) > 0)
                        return true;
                    else
                        return false;


                case Date2_Greater_Date1:

                    if (d2.compareTo(d1) > 0)
                        return true;
                    else
                        return false;

                case Date1_Equal_Date2:

                    if (d2.compareTo(d1) == 0)
                        return true;
                    else
                        return false;


            }
        }
        catch (Exception e) {}

        return false;
    }

    /**
     * Build a date as yyyy-mm-dd
     * @param year
     * @param month
     * @param dayOfMonth
     * @return String
     */
    public static String formatDate(int year, int month, int dayOfMonth) {

        return String.format("%04d", year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", dayOfMonth);
    }

    /**
     * Build a date as yyyy-mm-dd
     * @param date as Date
     * @return String
     */
    public static String formatDate(Date date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        return sdf.format(date);

    }

    /**
     * Build a time as hh:mm:ss ampm
     * @param hour
     * @param minute
     * @param second
     * @param ampm
     * @return time
     */

    public static String formatDate(int hour, int minute, int second, String ampm) {


        return String.format("%02d", hour) + ":" + String.format("%02d", minute) + ":" + String.format("%02d", second) + " " + ampm.toUpperCase();
    }

    /**
     * Take a time string and return in the format of HH:MM
     * @param time - passed in the format of HH:MM:SS AMPM
     * @return
     */
    public static String convertDateTo24Hour(String time) {

        if (time == null)
            return null;

        DateFormat inFormat = new SimpleDateFormat("hh:mm:ss aa");
        DateFormat outFormat = new SimpleDateFormat("HH:mm:ss");

        Date date = null;

        String formattedDate = null;
        try {
            date = inFormat.parse(time);
        } catch (ParseException e) {

        }

        if (date != null)
            formattedDate = outFormat.format(date);

        return formattedDate;

    }

    /**
     * Take a 24 hour time string and return in the format of hh:mm:ss: ampm
     * @param time - passed in the format of HH:mm:ss
     * @return
     */
    public static String convert24HourToDate(String time) {

        if (time == null)
            return null;

        DateFormat outFormat = new SimpleDateFormat("hh:mm:ss aa");
        DateFormat inFormat = new SimpleDateFormat("HH:mm:ss");

        Date date = null;

        String formattedDate = null;
        try {
            date = inFormat.parse(time);
        } catch (ParseException e) {

        }

        if (date != null)
            formattedDate = outFormat.format(date);

        return formattedDate;

    }

    /**
     * parse a  date formatted as yyyy-mm-dd
     * @param date - date to parse
     * @param datePart - YYYY, MM, DD
     * @return - year, month or day, or -1 if otherwise
     */
    public static int parseDate(String date, DatePart datePart) {

        if (!isValidDate(date))
            return -1;

        switch (datePart) {

            case YYYY:
                return Integer.parseInt(date.substring(0, 4));
            case MM:
                return Integer.parseInt(date.substring(5, 7));
            case DD:
                return Integer.parseInt(date.substring(8, 10));
        }

        return - 1;
    }

    /**
     * parse a  time formatted as hh:mm:ss ampm
     * @param time - time to parse
     * @param datePart - hh, mmm, ss, ampm
     * @return - part as a String, otherwise null
     */
    public static String parseTime(String time, DatePart datePart) {

        if (validateOrConvertTime(time) == null)
            return null;

        switch (datePart) {

            case HH:
                return time.substring(0, 2);
            case MMM:
                return time.substring(3, 5);
            case SS:
                return time.substring(6, 8);
            case AMPM:
                return time.substring(9, 11);
        }

        return null;
    }

    /**
     * Validate the passed date String is valid.  If not, see if it's close and convert to format yyyy-MM-dd
     * @param date
     * @return valid date in corect format, or null;
     */
    public static String validateOrConvertDate(String date) {

        String n = null;

        //if any pf the patterns fail, try to convert, oherwise just return the number
        if (Pattern.matches(DATE_REGEX_V1, date))
            n = date;
        else {

            //try to convert the number into a valid pattern
            if (date.matches(DATE_REGEX_V1_CONVERT)) {
                n = date.replaceFirst(DATE_REGEX_V1_CONVERT, "$1-$3-$4");
            }
            else
                if (date.matches(DATE_REGEX_V2_CONVERT)) {
                    n = date.replaceFirst(DATE_REGEX_V2_CONVERT, "$3-$1-$2");
            }
        }

        //Now validate the string built is a valid date.  For example, the string may have built 2015-09-31, which is not valida

        if (isValidDate(n)) {

            //date is valid, but it may look like 2015-1-1, so convert to 2015-01-01
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date d = format.parse(n);
                return format.format(d);
            }
            catch(ParseException e) {
                return null;
            }

        }
        else
            return null;
    }

    /**
     * Validate the passed time String is valid.  If not, see if it's close and convert to format hh:mmm:ss ampm
     * @param time
     * @return valid time in corect format, or null;
     */
    public static String validateOrConvertTime(String time) {

        String n = null;

        //if any pf the patterns fail, try to convert, oherwise just return the number
        if (Pattern.matches(TIME_REGEX_V1, time))
            return time;

        //no conversion implemented for this function
        return null;

    }

    /**
     * Simple reoutine to check if the passed date is a valid date using format yyyy-MM-dd.  Not only
     * validates format, but also if its a true date.  ie, 2015-09-31 would return false.
     * @param date
     * @return true if a valid date, flase otherwise
     */
    public static boolean isValidDate(String date) {

        if (date == null)
            return false;


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);

        //Exception will be thrown for an invalid date, otherwise return the date
        try {
            Date dtParse = sdf.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }


    }

    /**
     * validates a passed phone number string is in phone number format.  If so, it returns the passed phone number.
     * If not, it will attempt to convert to a phone number and return it.  If all else failse, it returns null
     *
     * @param number phone number to validate and/or convert
     * @return properly formatted phone number, or null if invalid and unconvertable
     */
    public static String validateOrConvertPhoneNumber(String number) {


        //if any pf the patterns fail, try to convert, oherwise just return the number
        if (Pattern.matches(PHONE_REGEX_V1, number) || Pattern.matches(PHONE_REGEX_V2, number) ||
                Pattern.matches(PHONE_REGEX_V3, number) || Pattern.matches(PHONE_REGEX_V4, number) ||
                Pattern.matches(PHONE_REGEX_V5, number))

            return number;
        else
            //try to convert the number into a valid pattern
            if (number.matches(PHONE_REGEX_V1_CONVERT)) {
                String n = number.replaceFirst(PHONE_REGEX_V1_CONVERT, "$1-$2");
                return n;
            } else if (number.matches(PHONE_REGEX_V2_CONVERT)) {
                String n = number.replaceFirst(PHONE_REGEX_V2_CONVERT, "$1-$2-$3");
                return n;
            } else if (number.matches(PHONE_REGEX_V3_CONVERT) && number.substring(0, 1).equals("1")) {
                String n = number.replaceFirst(PHONE_REGEX_V3_CONVERT, "$1-$2-$3-$4");
                return n;
            } else if (number.matches(PHONE_REGEX_V4_CONVERT)) {
                String n = number.replaceFirst(PHONE_REGEX_V4_CONVERT, "$1-$2-$3");
                return n;
            } else if (number.matches(PHONE_REGEX_V5_CONVERT) && number.substring(0, 1).equals("1")) {
                String n = number.replaceFirst(PHONE_REGEX_V5_CONVERT, "$1-$2-$3-$4");
                return n;
            } else

                return null;
    }

    /**
     * method will shift the popup menu to the left of the passed view
     *
     * @param popup menu to shift
     * @param view  anchored view to shift to left of
     */
    public static void gravitatePopUpWindowBottomLeft(PopupMenu popup, View view) {


        // Try to force some horizontal offset
        try {

            Field fMenuHelper = PopupMenu.class.getDeclaredField("mPopup");
            fMenuHelper.setAccessible(true);
            Object menuHelper = fMenuHelper.get(popup);
            Class[] argTypes;

            Field fListPopup = menuHelper.getClass().getDeclaredField("mPopup");
            fListPopup.setAccessible(true);
            Object listPopup = fListPopup.get(menuHelper);
            argTypes = new Class[]{int.class};
            Class listPopupClass = listPopup.getClass();

            // Get the width of the popup window
            int width = (Integer) listPopupClass.getDeclaredMethod("getWidth").invoke(listPopup);

            // Invoke setHorizontalOffset() with the negative width to move left by that distance
            listPopupClass.getDeclaredMethod("setHorizontalOffset", argTypes).invoke(listPopup, -width + view.getWidth());


            // Invoke show() to update the window's position
            listPopupClass.getDeclaredMethod("show").invoke(listPopup);
        } catch (Exception e) {
            // Again, an exception here indicates a programming error rather than an exceptional condition
            // at runtime
            Log.w("TAG", "Unable to force offset", e);
        }

    }

    /**
     * method will shift the popup menu to the left of the passed view
     *
     * @param popup menu to shift
     * @param view  anchored view to shift to left of
     */
    public static void gravitatePopUpWindowBottom(PopupMenu popup, View view, int offsetFromLeft) {


        // Try to force some horizontal offset
        try {

            Field fMenuHelper = PopupMenu.class.getDeclaredField("mPopup");
            fMenuHelper.setAccessible(true);
            Object menuHelper = fMenuHelper.get(popup);
            Class[] argTypes;

            Field fListPopup = menuHelper.getClass().getDeclaredField("mPopup");
            fListPopup.setAccessible(true);
            Object listPopup = fListPopup.get(menuHelper);
            argTypes = new Class[]{int.class};
            Class listPopupClass = listPopup.getClass();

            // Get the width of the popup window
            int width = (Integer) listPopupClass.getDeclaredMethod("getWidth").invoke(listPopup);

            // Invoke setHorizontalOffset() with the negative width to move left by that distance
            listPopupClass.getDeclaredMethod("setHorizontalOffset", argTypes).invoke(listPopup, offsetFromLeft);


            // Invoke show() to update the window's position
            listPopupClass.getDeclaredMethod("show").invoke(listPopup);
        } catch (Exception e) {
            // Again, an exception here indicates a programming error rather than an exceptional condition
            // at runtime
            Log.w("TAG", "Unable to force offset", e);
        }

    }


    /**
     * Format the passed URL to look like an HTML string, including http:// header and underine
     * @param source - URL to format as an HTML string
     * @return
     */
    public static CharSequence returnHTMLLinkString(String source) {

        source = source.toLowerCase();
        if (!source.startsWith("http://") && !source.startsWith("https://"))
            source = "http://" + source;

        return Html.fromHtml("<p><u>" + source + "</u></p>");

    }

    /**
     * Get total pixels of devce
     * @param act - any activity associated with the device
     * @return
     */
    public static int getDeviceWidth(Activity act) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) act.getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;

      }

    /**
     * Get total pixels of height
     * @param act - any activity associated with the device
     * @return
     */
    public static int getDeviceHeight(Activity act) {


        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) act.getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    /**
     * Returns the passed string as an underlined HTML format.  Does not add the http://
     * @param source - URL to format underlined
     * @return
     */
    public static CharSequence returnHTMLLinkAppearance(String source) {

        return Html.fromHtml("<u>" + source + "</u>");

    }

    /*
    ** Take a string with html code and formats for display
     */
    public static CharSequence returnHTMLFormattedString(String source) {
        return Html.fromHtml(source);
    }

    /**
     * Convert Density Independent Pixels to Pixels
     * @param act - activity
     * @param DPValue - DIP value
     * @return
     */
    public static int convertDPtoPX(Activity act, int DPValue) {

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DPValue, act.getResources().getDisplayMetrics());
    }

    /**
     * Convert Density Independent Pixels to Pixels
     * @param act - activity
     * @param SPValue - SP value
     * @return
     */
    public static int convertSPtoPX(Activity act, int SPValue) {

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, SPValue, act.getResources().getDisplayMetrics());
    }

    /**
     * Return a string of a double in the passed format.
     *
     * <p>Symbol     Location     Localized?     Meaning </p>
     *
     * <p>0 Digit<nbsp></p>
     * <p># Digit, zero shows as absent
     * <p>. Decimal separator or monetary decimal separator
     * <p>- Minus sign
     * <p>, Grouping separator
     * <p>% Multiply by 100 and show as percentage
     * <p>' Used to quote special characters in a prefix or suffix, for example, "'#'#" formats 123 to "#123". To create a single quote itself, use two in a row: "# o''clock".

     * @param value - value to format
     * @param format -Exmaples "###.##" formats as 123.45
     *
     *
     * @return - Formatted String
     */
    public static String formatDecimal(double value, String format) {

        if (Double.isNaN(value))
            return "";

        DecimalFormat myFormatter = new DecimalFormat(format);

        return myFormatter.format(value);

    }

    /**
     * Merhod will lock or unlock the current screen orientation
     * @param act - Activity
     * @param lock - true to lock, false to unlock
     */
    public static void lockScreenOrientation(Activity act, boolean lock) {

        int orient = act.getRequestedOrientation();

        if (lock) {

            switch (act.getResources().getConfiguration().orientation){
                case Configuration.ORIENTATION_PORTRAIT:
                    if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.FROYO){
                        act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    } else {
                        int rotation = act.getWindowManager().getDefaultDisplay().getRotation();
                        if(rotation == android.view.Surface.ROTATION_90|| rotation == android.view.Surface.ROTATION_180){
                            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                        } else {
                            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        }
                    }
                    break;

                case Configuration.ORIENTATION_LANDSCAPE:
                    if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.FROYO){
                        act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    } else {
                        int rotation = act.getWindowManager().getDefaultDisplay().getRotation();
                        if(rotation == android.view.Surface.ROTATION_0 || rotation == android.view.Surface.ROTATION_90){
                            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        } else {
                            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                        }
                    }
                    break;
            }
        }
        else
            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);


    }


    /**
     * Checks if the app has permission to write to device storage.   The calling method should check the return result.
     * If the result is PackageManager.PERMISSION_GRANTED, just continue on with opeation in normal flow
     * If the result is PackageManager.PERMISSION_DENIED, then a dialog box will be shown (by requestPermissions) asking user for permission.  Result wil be returned in onRequestPermissionsResult
     *    of the calling fragment
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static int verifyStoragePermissions(Activity activity, Fragment retFrag) {

        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {

            // We don't have permission so prompt the user.  Result should go back to calling fragment in onRequestPermissionsResult
            retFrag.requestPermissions(
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }

        return permission;
    }

    /**
     * Simple method to compare equality of two strings.  If both are nulls, they are considered equal.
     * @param str1
     * @param str2
     * @return true if strings are duplicates
     */
    public static boolean compareStringForEquality(String str1, String str2) {

        boolean flDup1;

        //compare fromValue
        if (str1 == null && str2 == null)
            flDup1 = true;
        else
            if ((str1 == null && str2 != null) || (str1 != null && str2 == null))
                flDup1 = false;
            else
                if (str1.equals(str2))
                    flDup1 = true;
                else
                    flDup1 = false;

        return flDup1;



    }

    /**
     * Returns true if passed string is alpha only
     * @param val
     * @return
     */
    public static boolean isAlphaOnly(String val) {

        return !val.matches("[0-9]+");
    }
}

