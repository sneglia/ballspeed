package evets.neg.ballspeed;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import evets.neg.ballspeed.exceptionHandling.tbeException;


/**
 * Created by Steve on 8/10/2015.
 * This is the base class for DB helping.  It is declasred abstract so it can not be instantiated.
 * Users must call the getInstance method of DBhelper for simple transactions, ot DBHelperTransaction for
 * processing requiring multiple trsnactions before a commit.
 */
public abstract class DBHelperBase extends SQLiteOpenHelper {

    // database version, locatedin AppInfo class
    public static final int DB_VERSION = AppInfo.DB_VERSION;

    // Database Information
    public static final String DB_NAME = AppInfo.DB_NAME;
    public static final String DB_NAME_BACKUP = AppInfo.DB_NAME_BACKUP;

    //DB Logging.  Use "ON" or "OFF"
    public  static final String DB_DEBUG_LOGGING = "OFF";

    //Value is the initial header value for SQLitr files
    private static final String SQLLITE_INITIAL_HEADER_CHARS = "SQLite format";

    private final Context context;

    //SQLLITE specific constants
    public static String SQLLITE_TRUE_VALUE = "1";
    public static String SQLLITE_FALSE_VALUE = "0";

    //Helper vaues fr non-passed numbers
    public static int IGNORE_PASSED_VALUE = -1;         //use this field for db update to ignore value passed in
    public static int SET_AS_NULL_VALUE = -2;           //use this to set a numeric value in db as null
    public static String SET_STRING_AS_NULL_VALUE = "_NULL_";    //use this to set a String as a null value.  Use when a real null is ignored by DB command.

    //This should not be called implicitely.  It is only public so this class can be inherited
    // Use getInstance to get an instance of this object.
    public DBHelperBase(Context context) {

        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;

    }

    //This should not be called implicitely.  It is only public so this class can be inherited
    //Call using a DBHelp obeject
    //This will be called for a specific database name, and not the default database
    public DBHelperBase(Context context, String dbName) {

        super(context, dbName, null, DB_VERSION);
        this.context = context;

    }

    //abstract methods
    public abstract boolean inTransactionMode();
    public abstract SQLiteDatabase getSQLDB();
    public abstract void closeDB();

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Only call this to create the initial version of the database.
        //Specify all versions to handle
        //1) The initial install of version 1 of this program
        //2) The initial install for new uses of this program (they need all updates)

        try {
            executeDBXML(db, 1, 99999);
        }
        catch (tbeException e) {
            throw new RuntimeException(e.getErrorMessage());
        }
        catch (Throwable e)
        {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //execute any new db updates between the old version and the new version
        try {
            executeDBXML(db, oldVersion + 1, newVersion);
        }
        catch (tbeException e) {

            throw new RuntimeException(e.getErrorMessage());
        }
        catch (Throwable e)
        {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }

    //Return database vesrion
    public int getDBVersion() {

        SQLiteDatabase sql = getReadableDatabase();
        int v = sql.getVersion();
        sql.close();
        return v;
    }

    /**
     * Determines if passed file is a database name
     * @param fileName - name of file to validate
     * @return - true if  the file is a SQLite database, false otherwise
     */
    public static boolean isDatabaseSQLite(String fileName) {

        String header = null;

        try {
            byte[] buffer = new byte[SQLLITE_INITIAL_HEADER_CHARS.length()];
            FileInputStream file = new FileInputStream(fileName);
            file.read(buffer, 0, SQLLITE_INITIAL_HEADER_CHARS.length());

            if (buffer != null)
                header = new String(buffer);
            else
                return false;

            if (header.equals(SQLLITE_INITIAL_HEADER_CHARS))
                return true;
        }
        catch (Exception e) {
            return  false;
        }

        return false;

    }

    //Read the db_sql file and execute all SQL statements beginning from the passed version number up to passed TO version number
    public boolean executeDBXML(SQLiteDatabase db, int fromVersionNo, int toVersionNo) throws Exception {

        AssetManager am = context.getAssets();
        InputStream is = am.open("db_sql.xml");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuild = dbf.newDocumentBuilder();
        Document doc = dBuild.parse(is);

        doc.getDocumentElement().normalize();
        NodeList nodeLst = doc.getElementsByTagName("Version");

        for (int s = 0; s < nodeLst.getLength(); s++) {

            Node fstNode = nodeLst.item(s);

            //pull the Version Number associated with the Version Element from the value attribute
            int versionNo = Integer.parseInt(fstNode.getAttributes().getNamedItem("value").getNodeValue());

            //if the Version element is equal to or greater than the version number passed, execute the SQL statement
            if ( versionNo >= fromVersionNo && versionNo <= toVersionNo ) {

                if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element stateElement = (Element) fstNode;
                    NodeList statElementList = stateElement.getElementsByTagName("Statement");
                    String SQL;

                    for (int i=0;i<statElementList.getLength(); i++) {
                        SQL = statElementList.item(i).getChildNodes().item(0).getNodeValue().replace("\n","").replace("\t"," ").trim();

                        if (SQL.length()>0)

                            //try to execute the SQL.  If it fails, catch it and reformat as a tbeException to format a pretty error message
                            try {
                                db.execSQL(SQL);
                            }
                            catch (Exception e) {
                                throw (new tbeException(e));
                            }

                    }

                }
            }

        }
        return true;
    }

    /**
     * Call this method right after any insert to return the last autoincrement _id that was inserted
     * @return
     */
    public int getLastInsertedID() {

            int cnt = 0;

            //SQLiteDatabase sqlDB = getReadableDatabase();
            SQLiteDatabase sqlDB = getSQLDB();

            String sqlStatement = "select last_insert_rowid()";
            Cursor c = sqlDB.rawQuery(sqlStatement, null);

            c.moveToFirst();
            cnt = c.getCount();

            int retCnt;

            if (cnt > 0)
                retCnt =  c.getInt(0);
            else
                retCnt =  0;

            closeDB();

            return  retCnt;
    }

    /**
     * Method returns a fully executable SQL string by replacing the ? with paramaters from args array.
     * @param sql - fully prepared SQL statement with ? as args
     * @param args - array of args to use to replace ? in SQL
     * @return
     */
    public String buildExecutableSQL(String sql, String[] args) {

        String retString = sql;

        for (int i = 0; i < args.length; i++) {

            retString = retString.replaceFirst("\\?", args[i]);

        }
        return  retString;
    }

    /**
     * Log to logcat SQL statement if logging is turned on
     * @param sql
     * @param args
     */
    public void logSQL(String sql, String[] args, String location) {

        if (DB_DEBUG_LOGGING.equals("ON"))
            Log.d("SQL_DEBUG", "*** EXECUTED SQL IN "+ location + "//" + Thread.currentThread().getStackTrace()[3].getMethodName()+" *** " +buildExecutableSQL(sql, args));

    }

}
