package evets.neg.ballspeed;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import static evets.neg.ballspeed.CustomToast.CUSTOM_TOAST_GRAVITY;


public class BowlerActivity extends AppCompatActivity {

    //Window fields
    final Context context = this;

    public static final String MODE_MESSAGE_CODE = "MESSAGE_CODE";

    private EditText edtFName;
    private EditText edtLName;
    private Spinner lstBowlers;
    private ImageButton butDelete;
    private ImageButton butAdd;
    private ImageButton butUpdate;

    private int bowlerID;
    private int bowlerLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Do not close window if dialog is touched outside bounds
        setFinishOnTouchOutside(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_bowler);


        //Reference to fields
        lstBowlers = (Spinner) findViewById(R.id.lstBowler);
        edtFName = (EditText) findViewById(R.id.edtFName);
        edtLName = (EditText) findViewById(R.id.edtLName);
        butAdd = (ImageButton) findViewById(R.id.butAddBowler);
        butUpdate = (ImageButton) findViewById(R.id.butUpdateBowler);
        butDelete = (ImageButton) findViewById(R.id.butDeleteBowler);

        //Handle when distance is clicked
        lstBowlers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (view == null)
                    return;

                bowlerLocation = position;
                bowlerID =  Integer.parseInt(((TextView) view.findViewById(R.id.txtCodeG1)).getText().toString());
                edtFName.setText(((TextView) view.findViewById(R.id.txtCodeG2)).getText().toString());
                edtLName.setText(((TextView) view.findViewById(R.id.txtCodeG3)).getText().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        butAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                insertBowler();
            }
        });

        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteBowler();
            }
        });

        butUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateBowler();
            }
        });


        //Save Instance state variables;
        if (savedInstanceState != null) {

            bowlerID = savedInstanceState.getInt("bowlerID");
            bowlerLocation = savedInstanceState.getInt("bowlerLocation");

        }

    }

    /**
     * Save all variables needing restoration later
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Saving variables
        savedInstanceState.putInt("bowlerID", bowlerID);
        savedInstanceState.putInt("bowlerLocation", bowlerLocation);

        // Call at the end
        super.onSaveInstanceState(savedInstanceState);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        //MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            //help was clicked
            case R.id.menu_quick_help:

                //Intent i = new Intent(this, QuickHelpDialogActivity.class);
                //i.putExtra(QuickHelpDialogActivity.MODE_MESSAGE_CODE, QuickHelpDialogActivity.START_MODE_MEASURE_BALL_SPEED);
                //startActivity(i);
                //break;

        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Update title bar
     * @param title
     * @param subTitle
     */
    public void updateTitle(String title, String subTitle) {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);
        actionBar.setSubtitle(subTitle);
    }


    @Override
    public void onResume() {

        super.onResume();

        //Load all the bowlers
        loadBowlers();


    }

    /**
     * Load the Bowlers Spinner
     */
    private void loadBowlers() {

        String[] lname = null;
        String[] fname = null;
        String[] fullName = null;
        String[] ID = null;
        int cnt = 0;

        //Attach to DB
        DB_BowlerTable sqlDB = new DB_BowlerTable(DBhelper.getInstance(this));

        //Execute query
        ArrayList<HashMap<String, String>> baTable = (ArrayList<HashMap<String, String>>) sqlDB.getBowlersTable();

        //For each row of the return ArrayList, extract the Map object and pull out data into arrays
        if (baTable != null && baTable.size() > 0) {

            cnt = baTable.size();

            //Make room for all returned rows and 1 empty row for No Bowler
            lname = new String[cnt+1];
            fname = new String[cnt+1];
            fullName = new String[cnt+1];
            ID = new String[cnt+1];

            for (int i = 0; i < cnt; i++) {
                HashMap<String, String> map = baTable.get(i);
                lname[i] = map.get(DB_BowlerTable.COL_LNAME);
                fname[i] = map.get(DB_BowlerTable.COL_FNAME);
                ID[i] = map.get(DB_BowlerTable.COL_ID);

                //minimum field required
                fullName[i] = fname[i];

                //did they supply a last name, then include
                if (lname[i] != null && lname[i].length() !=0)
                    fullName[i] += " " + lname[i];

            }
        }

        //Fill Empty Row
        if (cnt == 0) {
            fullName = new String[1];
            ID = new String[1];
            lname = new String[1];
            fname = new String[1];
            fullName[0] = "<No available bowlers>";
            ID[0] = "0";
            lname[0] = "";
            fname[0] = "";
        }
        else {
            fullName[cnt] = "<Select bowler>";
            ID[cnt] = "0";
            lname[cnt] = "";
            fname[cnt] = "";
        }

        //Load the listview adapter with Mainfield and hiddene elemnsts
        AdapterGenericWithCodes adapter = new AdapterGenericWithCodes(this, fullName, ID, fname, lname, null, null, null, null, null, null, null, R.layout.list_row_generic_10_codes);
        lstBowlers.setAdapter(adapter);
        adapter.addHeaderRow("Select Bowler", R.layout.list_row_generic_codes_group_header_row);
        adapter.notifyDataSetChanged();

        //If we found bowlers, select last one selected if we have one
        if (cnt != 0) {

            //select last bowler selected, else empty row
            if (bowlerID != 0)
                lstBowlers.setSelection(bowlerLocation);
            else
                lstBowlers.setSelection(cnt+1);
                adapter.setPositionState(cnt+1,false, false);
        }

        //There's no beolwers, so select the empty row
        else {
            adapter.setPositionState(1,true, false);
            lstBowlers.setSelection(1);
        }


    }

    private void insertBowler() {

        String lname = edtLName.getText().toString().trim();
        String fname = edtFName.getText().toString().trim();

        //Validate as least a first name
        if (fname.length() == 0) {
            MessageBox.showOKMessageBoxRequiredFieldMissing(this, "First Name");
            return;
        }

        //Make sure this name does not exist
        DBHelperTransaction db = DBHelperTransaction.getInstance(this);
        DB_BowlerTable sqlDB = new DB_BowlerTable(db);

        //Execute query to look for exact match. This means if only a first name is entered, it is OK if another bowler has the same first name
        //but if the fname, last name and/or nname all match, something has to change to uniquely identify this bowler
        ArrayList<HashMap<String, String>> baTable = (ArrayList<HashMap<String, String>>) sqlDB.checkExistenceBowler(lname, fname, false);

        if (baTable != null && baTable.size() > 0) {

            HashMap<String, String> map = baTable.get(0);
            int ID =  Integer.parseInt(map.get(DB_BowlerTable.COL_ID));

            // if a row was found and it's not the same bowlers, display validation error and return
            MessageBox.showOKMessageBoxValidationError(this, "A bowler already exists with this first name and last name. Please modify to create a unique identity.");
            return;

        }

        //INsert row
        db.beginTransactionDB();
            sqlDB.insertBowlerRecord(lname, fname);
            int lastID = db.getLastInsertedID();
        db.commitTransactioDB();

        //reload bowlers
        loadBowlers();

        //Find the new name in the list
        AdapterGenericWithCodes adapter = (AdapterGenericWithCodes) lstBowlers.getAdapter();
        int pos = adapter.getItem(lastID+"", AdapterGenericWithCodes.GroupBy.Code1);
        if (pos != AdapterGenericWithCodes.ITEM_NOT_FOUND)
            lstBowlers.setSelection(pos);


        //And toast the insert

        CustomToast customToast = new CustomToast();
        customToast.setGravity(Gravity.BOTTOM, 0, CUSTOM_TOAST_GRAVITY);
        customToast.toast(this, R.layout.custom_toast_layout, R.id.textToShow, "Bowler Added.");


    }

    private void updateBowler() {

        String lname = edtLName.getText().toString().trim();
        String fname = edtFName.getText().toString().trim();

        //Validate as least a first name
        if (bowlerID == 0) {
            MessageBox.showOKMessageBoxValidationError(this, "Please select bowler to update.");
            return;
        }

        if (fname.length() == 0) {
            MessageBox.showOKMessageBoxRequiredFieldMissing(this, "First Name");
            return;
        }

        //Make sure this name does not exist
        DBHelperTransaction db = DBHelperTransaction.getInstance(this);
        DB_BowlerTable sqlDB = new DB_BowlerTable(db);

        //Execute query to look for exact match. This means if only a first name is entered, it is OK if another bowler has the same first name
        //but if the fname, last name and/or nname all match, something has to change to uniquely identify this bowler
        ArrayList<HashMap<String, String>> baTable = (ArrayList<HashMap<String, String>>) sqlDB.checkExistenceBowler(lname, fname, false);

        if (baTable != null && baTable.size() > 0) {

            HashMap<String, String> map = baTable.get(0);
            int ID =  Integer.parseInt(map.get(DB_BowlerTable.COL_ID));

            // if a row was found and it's not the same bowlers, display validation error and return
            if (ID != bowlerID) {
                MessageBox.showOKMessageBoxValidationError(this, "A bowler already exists with this first name and last name. Please modify to create a unique identity.");
                return;
            }

        }

        //INsert row
        db.beginTransactionDB();
            sqlDB.updateBowlerRecord(bowlerID, lname, fname);
        db.commitTransactioDB();

        //reload bowlers
        loadBowlers();

        //Find the new name in the list
        AdapterGenericWithCodes adapter = (AdapterGenericWithCodes) lstBowlers.getAdapter();
        int pos = adapter.getItem(bowlerID+"", AdapterGenericWithCodes.GroupBy.Code1);
        if (pos != AdapterGenericWithCodes.ITEM_NOT_FOUND)
            lstBowlers.setSelection(pos);


        //And toast the update

        CustomToast customToast = new CustomToast();
        customToast.setGravity(Gravity.BOTTOM, 0, CUSTOM_TOAST_GRAVITY);
        customToast.toast(this, R.layout.custom_toast_layout, R.id.textToShow, "Bowler Updated.");


    }

    private void deleteBowler() {

        //Validate as bowler is selected
        if (bowlerID == 0) {
            MessageBox.showOKMessageBoxValidationError(this, "Please select bowler to delete.");
            return;
        }

        final Activity act = this;

        //show first warning message of delete
        MessageBox.ShowMultiButtonMessageBox(this, R.style.simpleDialogBoxStyle, "Deleting selected bowler will remove all bowler history.",
                "Warning!", new String[]{"Continue?", "Cancel"}, new DialogInterface.OnClickListener() {

                    //Evaluate the button clicked
                    public void onClick(DialogInterface dialog, int id) {

                        switch (id) {

                            //if button to delete was clicked, give another warning
                            case DialogInterface.BUTTON_POSITIVE:

                                //Make sure this name does not exist
                                DBHelperTransaction db = DBHelperTransaction.getInstance(act);
                                DB_BowlerTable sqlDB = new DB_BowlerTable(db);
                                DB_HistoryTable sqlDB2 = new DB_HistoryTable(db);

                                db.beginTransactionDB();
                                    sqlDB.deleteBowlerRecord(bowlerID);
                                    sqlDB2.deleteHistoryRecord(bowlerID, null);
                                db.commitTransactioDB();

                                //reload bowlers
                                bowlerID = 0;
                                bowlerLocation = 0;
                                loadBowlers();

                                //And toast the delete
                                CustomToast customToast = new CustomToast();
                                customToast.setGravity(Gravity.BOTTOM, 0, CUSTOM_TOAST_GRAVITY);
                                customToast.toast(act, R.layout.custom_toast_layout, R.id.textToShow, "Bowler Deleted.");
                        }

                    }
                });









    }

    /**
     * OnDestroy
     */
    public void onDestroy() {

        super.onDestroy();

    }



}


