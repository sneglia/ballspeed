package evets.neg.ballspeed;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;

import java.util.ArrayList;
import java.util.HashMap;

import static evets.neg.ballspeed.CustomToast.CUSTOM_TOAST_GRAVITY;


public class HistoryActivity extends AppCompatActivity {

    //Window fields
    final Context context = this;

    public static final String MODE_MESSAGE_CODE = "MESSAGE_CODE";

    private Spinner lstBowlers;
    private DateEditText dateFrom;
    private DateEditText dateTo;
    private Button butFetch;
    private ListView lstResults;
    private RelativeLayout RL_Filter;
    private Switch swAll;
    private Switch swHits;
    private Switch swMisses;
    private TextView txtRowsReturned;
    private TextView txtHitsReturned;
    private TextView txtMissesReturned;

    private int bowlerID;
    private int bowlerLocation;
    private String resultsTimestamp;
    private int resultsLocation;
    private Menu menu;
    private int rowsReturned;
    private int hitsReturned;
    private int missesReturned;

    //Results return values.  Keep global to allow for rotation
    private String[] dateTime;
    private String[] timeStamp;
    private String[] targetActualSpeed;
    private String[] deviation;
    private String[] seconds;
    private String[] hitMiss;
    private String[] ID;

    private boolean flRotate;
    private boolean rlIsShown;
    private boolean justInSetSwitches;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        FirebaseCrash.log("In HistoryActivity / OnCreate");

        //Do not close window if dialog is touched outside bounds
        //setFinishOnTouchOutside(false);
        //supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_history);


        //Reference to fields
        lstBowlers = (Spinner) findViewById(R.id.lstBowlers);
        dateFrom = (DateEditText) findViewById(R.id.dateFrom);
        dateTo = (DateEditText) findViewById(R.id.dateTo);
        butFetch = (Button) findViewById(R.id.butFetchData);
        lstResults = (ListView) findViewById(R.id.lstHistory);
        RL_Filter = (RelativeLayout) findViewById(R.id.RL_Filter);
        swAll = (Switch) findViewById(R.id.showAll);
        swMisses = (Switch) findViewById(R.id.showOnlyMisses);
        swHits = (Switch) findViewById(R.id.showOnlyHits);
        txtRowsReturned = (TextView) findViewById(R.id.txtRowsReturned);
        txtHitsReturned = (TextView) findViewById(R.id.txtHitsReturned);
        txtMissesReturned = (TextView) findViewById(R.id.txtMissesReturned);

        //Handle when bowler is clicked
        lstBowlers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (view == null)
                    return;

                bowlerLocation = position;
                bowlerID =  Integer.parseInt(((TextView) view.findViewById(R.id.txtCodeG1)).getText().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lstResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (view == null)
                    return;

                resultsLocation = i;
                resultsTimestamp =  ((TextView) view.findViewById(R.id.txtCodeG6)).getText().toString();

                MenuItem m = menu.findItem(R.id.menu_history_delete_row);
                m.setVisible(true);
            }
        });


        //Fetch data

        butFetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bowlerID <=0 )
                    MessageBox.showOKMessageBoxRequiredFieldMissing(context, "\n\nBowler Name");
                else {

                    resultsTimestamp =  null;
                    MenuItem m = menu.findItem(R.id.menu_history_delete_row);
                    m.setVisible(false);
                    loadResultsListView();
                }
            }
        });


        swAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swHits.setChecked(false);
                swMisses.setChecked(false);
            }
        });

        swHits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swAll.setChecked(false);
            }
        });

        swMisses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swAll.setChecked(false);
            }
        });

        //Save Instance state variables;
        if (savedInstanceState != null) {

            resultsLocation = savedInstanceState.getInt("resultsLocation");
            resultsTimestamp = savedInstanceState.getString("resultsTimestamp");
            bowlerID = savedInstanceState.getInt("bowlerID");
            bowlerLocation = savedInstanceState.getInt("bowlerLocation");
            dateTime = savedInstanceState.getStringArray("dateTime");
            timeStamp = savedInstanceState.getStringArray("timeStamp");
            targetActualSpeed = savedInstanceState.getStringArray("targetActualSpeed");
            deviation = savedInstanceState.getStringArray("deviation");
            seconds = savedInstanceState.getStringArray("seconds");
            hitMiss = savedInstanceState.getStringArray("hitMiss");
            ID = savedInstanceState.getStringArray("ID");
            rlIsShown = savedInstanceState.getBoolean("rlIsShown");
            rowsReturned = savedInstanceState.getInt("rowsReturned");
            hitsReturned = savedInstanceState.getInt("hitsReturned");
            missesReturned = savedInstanceState.getInt("missesReturned");

            if (rlIsShown)
                RL_Filter.setVisibility(View.VISIBLE);
            else
                RL_Filter.setVisibility(View.GONE);


            flRotate = true;

        }
        else {

            initializeHistoryVariables();
            flRotate = false;
            rlIsShown = false;
            RL_Filter.setVisibility(View.GONE);

        }

        updateTitle("Show History", null);
        showBackArrow();

    }

    /**
     * Save all variables needing restoration later
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Saving variables
        savedInstanceState.putInt("bowlerID", bowlerID);
        savedInstanceState.putInt("bowlerLocation", bowlerLocation);
        savedInstanceState.putInt("resultsLocation", resultsLocation);
        savedInstanceState.putString("timeStamp", resultsTimestamp);
        savedInstanceState.putString("dateFrom", dateFrom.getDate());
        savedInstanceState.putString("dateTo", dateTo.getDate());
        savedInstanceState.putStringArray("dateTime", dateTime);
        savedInstanceState.putStringArray("timeStamp", timeStamp);
        savedInstanceState.putStringArray("targetActualSpeed", targetActualSpeed);
        savedInstanceState.putStringArray("deviation", deviation);
        savedInstanceState.putStringArray("seconds", seconds);
        savedInstanceState.putStringArray("hitMiss", hitMiss);
        savedInstanceState.putStringArray("ID", ID);
        savedInstanceState.putBoolean("rlIsShown", rlIsShown);
        savedInstanceState.putInt("rowsReturned", rowsReturned);
        savedInstanceState.putInt("hitsReturned", hitsReturned);
        savedInstanceState.putInt("missesReturned", missesReturned);


        // Call at the end
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {

            //Since the view will reload its state, you need to load these here, after view state is loaded.
            dateFrom.refreshViewAfterRotate();
            dateTo.refreshViewAfterRotate();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_history, menu);
        this.menu = menu;

        //Reselect row on rotate.  DO this hear because menu needs a value
        if (resultsLocation != -1) {
            lstResults.setSelection(resultsLocation);
            lstResults.performItemClick(lstResults.getAdapter().getView(resultsLocation, null, null), resultsLocation, lstResults.getAdapter().getItemId(resultsLocation));
        }

        //If the user clicked to show the filter, show the stop, else show the regular filter
        if (rlIsShown)
            menu.findItem(R.id.menu_history_filter).setIcon(R.drawable.filter_stop);
        else
            menu.findItem(R.id.menu_history_filter).setIcon(R.drawable.filter);

        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                this.finish();
                return true;

            //help was clicked
            case R.id.menu_quick_help:

                Intent i = new Intent(this, QuickHelpDialogActivity.class);
                i.putExtra(QuickHelpDialogActivity.CODE_TITLE, "Show History Help");
                i.putExtra(QuickHelpDialogActivity.CODE_FILE_NAME, "qh_history.htm");
                startActivity(i);
                break;

            case R.id.menu_history_delete_row:
                deleteSelectedRow();
                break;

            case R.id.menu_history_filter:

                if (rlIsShown) {
                    rlIsShown = false;
                    RL_Filter.setVisibility(View.GONE);
                    menu.findItem(R.id.menu_history_filter).setIcon(R.drawable.filter);
                }
                else {
                    rlIsShown = true;
                    RL_Filter.setVisibility(View.VISIBLE);
                    menu.findItem(R.id.menu_history_filter).setIcon(R.drawable.filter_stop);
                }

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Update title bar
     * @param title
     * @param subTitle
     */
    public void updateTitle(String title, String subTitle) {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);
        actionBar.setSubtitle(subTitle);

    }

    /**
     * Update title bar
     */
    public void showBackArrow() {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public void onResume() {

        super.onResume();

        //Load all the bowlers
        loadBowlers();

        //If we have already loaded a results view, then show it
        if (dateTime != null)
            loadResultsListView();

    }


    /**
     * Load the Bowlers Spinner
     */
    private void loadBowlers() {

        String[] lname = null;
        String[] fname = null;
        String[] fullName = null;
        String[] ID = null;
        int cnt = 0;

        //Attach to DB
        DB_BowlerTable sqlDB = new DB_BowlerTable(DBhelper.getInstance(this));

        //Execute query
        ArrayList<HashMap<String, String>> baTable = (ArrayList<HashMap<String, String>>) sqlDB.getBowlersTable();

        //For each row of the return ArrayList, extract the Map object and pull out data into arrays
        if (baTable != null && baTable.size() > 0) {

            cnt = baTable.size();

            //Make room for all returned rows and 1 empty row for No Bowler
            lname = new String[cnt+1];
            fname = new String[cnt+1];
            fullName = new String[cnt+1];
            ID = new String[cnt+1];

            for (int i = 0; i < cnt; i++) {
                HashMap<String, String> map = baTable.get(i);
                lname[i] = map.get(DB_BowlerTable.COL_LNAME);
                fname[i] = map.get(DB_BowlerTable.COL_FNAME);
                ID[i] = map.get(DB_BowlerTable.COL_ID);

                //minimum field required
                fullName[i] = fname[i];

                //did they supply a last name, then include
                if (lname[i] != null && lname[i].length() !=0)
                    fullName[i] += " " + lname[i];

            }
        }

        //Fill Empty Row
        if (cnt == 0) {
            fullName = new String[1];
            ID = new String[1];
            lname = new String[1];
            fname = new String[1];
            fullName[0] = "<No available bowlers>";
            ID[0] = "0";
            lname[0] = "";
            fname[0] = "";
        }
        else {
            fullName[cnt] = "<Select bowler>";
            ID[cnt] = "0";
            lname[cnt] = "";
            fname[cnt] = "";
        }

        //Load the listview adapter with Mainfield and hiddene elemnsts
        AdapterGenericWithCodes adapter = new AdapterGenericWithCodes(this, fullName, ID, fname, lname, null, null, null, null, null, null, null, R.layout.list_row_generic_10_codes);
        lstBowlers.setAdapter(adapter);
        adapter.addHeaderRow("Select Bowler", R.layout.list_row_generic_codes_group_header_row);
        adapter.notifyDataSetChanged();

        //If we found bowlers, select last one selected if we have one
        if (cnt != 0) {

            //select last bowler selected, else empty row
            if (bowlerID != 0)
                lstBowlers.setSelection(bowlerLocation);
            else
                lstBowlers.setSelection(cnt+1);
                //adapter.setPositionState(cnt+1,false, false);
        }

        //There's no beolwers, so select the empty row
        else {
            adapter.setPositionState(1,true, false);
            lstBowlers.setSelection(1);
        }


    }


    /**
     * Load the History for the bowler
     */
    private void loadResultsListView() {

        String fromDate;
        String toDate;

        //Assume full length of time
        fromDate = dateFrom.getDate();
        toDate = dateTo.getDate();

        if (fromDate == null) {
            fromDate = "0000-00-00 00:00:000";
        }
        else
            fromDate += " 00:00:000";

        if (toDate == null) {
            toDate = "9999-12-31 23:59:999";
        }
        else
            toDate += " 23:59:999";

        //If we rotated, dateTime may have values, so do not refresh
        if (!flRotate) {

            int cnt = 0;

            //reset variables
            initializeHistoryVariables();

            //Attach to DB
            DB_HistoryTable sqlDB = new DB_HistoryTable(DBhelper.getInstance(this));

            //Execute query
            ArrayList<HashMap<String, String>> baTable = (ArrayList<HashMap<String, String>>) sqlDB.getHistoryTableUsingID(bowlerID, fromDate, toDate,
                                                          swAll.isChecked(), swHits.isChecked(), swMisses.isChecked());

            //For each row of the return ArrayList, extract the Map object and pull out data into arrays
            if (baTable != null && baTable.size() > 0) {

                cnt = baTable.size();
                rowsReturned = cnt;

                //Make room for all returned rows and 1 empty row for No Bowler
                dateTime = new String[cnt];
                timeStamp = new String[cnt];
                targetActualSpeed = new String[cnt];
                deviation = new String[cnt];
                seconds = new String[cnt];
                hitMiss = new String[cnt];
                ID = new String[cnt];

                for (int i = 0; i < cnt; i++) {


                    HashMap<String, String> map = baTable.get(i);

                    ID[i] = map.get(DB_HistoryTable.COL_ID);

                    //date/Time 2017-10-20 20:10:10:000
                    dateTime[i] = map.get(DB_HistoryTable.COL_TIMESTAMP).substring(0, 10) + "\\\n" + map.get(DB_HistoryTable.COL_TIMESTAMP).substring(11, 19);
                    timeStamp[i] = map.get(DB_HistoryTable.COL_TIMESTAMP);

                    //Target over actual speed
                    String actualSpeed = Utils.formatDecimal(Float.parseFloat(map.get(DB_HistoryTable.COL_MPH)), "###.##");
                    if (map.get(DB_HistoryTable.COL_TARGET_SPEED) != null) {
                        targetActualSpeed[i] = map.get(DB_HistoryTable.COL_TARGET_SPEED) + "\\\n" + actualSpeed;
                    } else
                        targetActualSpeed[i] = "N/A" + "\\\n" + actualSpeed;

                    //Deviation
                    deviation[i] = map.get(DB_HistoryTable.COL_DEVIATION);
                    if (deviation[i] == null)
                        deviation[i] = "N/A";

                    //Seconds
                    seconds[i] = Utils.formatDecimal(Float.parseFloat(map.get(DB_HistoryTable.COL_SECONDS)), "###.##");

                    //Hitmiss
                    hitMiss[i] = map.get(DB_HistoryTable.COL_IS_HIT);
                    if (hitMiss[i] == null)
                        hitMiss[i] = "N/A";
                    else
                        if (hitMiss[i].equals(DB_HistoryTable.IS_HIT_YES)) {
                            hitsReturned++;
                            hitMiss[i] = "HIT";
                        }
                        else
                            if (hitMiss[i].equals(DB_HistoryTable.IS_HIT_NO)) {
                                missesReturned++;
                                hitMiss[i] = "MISS";
                            }
                }
            }
        }

        //Load the listview adapter with Mainfield and hiddene elemnsts
        AdapterGenericWithCodes adapter = new AdapterGenericWithCodes(this, dateTime, targetActualSpeed, deviation, seconds, hitMiss, ID, timeStamp, null, null, null, null, R.layout.list_row_generic_10_codes_history_results);
        lstResults.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        //Reset rotate flag
        flRotate = false;

        //Set bottom fields
        int totalHitsAndMisses = hitsReturned + missesReturned;
        String percentHits="", percentMisses="";
        if (totalHitsAndMisses > 0) {
            percentHits = Utils.formatDecimal((float) hitsReturned/totalHitsAndMisses, "(##.#%)");
            percentMisses = Utils.formatDecimal((float) missesReturned/totalHitsAndMisses, "(##.#%)");
        }
        txtRowsReturned.setText("Rows Returned: " + rowsReturned);
        txtHitsReturned.setText("Hits: " + hitsReturned + " " + percentHits);
        txtMissesReturned.setText("Misses: " + missesReturned + " " + percentMisses);


    }


    private void deleteSelectedRow() {

        //Validate as bowler is selected
        final Activity act = this;

        //show first warning message of delete
        MessageBox.ShowMultiButtonMessageBox(this, R.style.simpleDialogBoxStyle, "Delete selected history record?.",
                "Warning!", new String[]{"Continue?", "Cancel"}, new DialogInterface.OnClickListener() {

                    //Evaluate the button clicked
                    public void onClick(DialogInterface dialog, int id) {

                        switch (id) {

                            //if button to delete was clicked, give another warning
                            case DialogInterface.BUTTON_POSITIVE:

                                //Make sure this name does not exist
                                DBhelper db = DBhelper.getInstance(act);
                                DB_HistoryTable sqlDB = new DB_HistoryTable(db);

                                //delete row
                                sqlDB.deleteHistoryRecord(bowlerID, resultsTimestamp);

                                //reset screen
                                menu.findItem(R.id.menu_history_delete_row).setVisible(false);
                                initializeHistoryVariables();

                                //and reload
                                loadResultsListView();

                                //And toast the delete
                                CustomToast customToast = new CustomToast();
                                customToast.setGravity(Gravity.BOTTOM, 0, CUSTOM_TOAST_GRAVITY);
                                customToast.toast(act, R.layout.custom_toast_layout, R.id.textToShow, "Record Deleted.");
                        }

                    }
                });

    }

    private void initializeHistoryVariables() {

        resultsLocation = -1;
        resultsTimestamp = null;
        dateTime = null;
        timeStamp = null;
        targetActualSpeed = null;
        deviation = null;
        seconds = null;
        hitMiss = null;
        ID = null;

        rowsReturned = hitsReturned = missesReturned = 0;

    }

    /**
     * OnDestroy
     */
    public void onDestroy() {

        super.onDestroy();

    }



}


