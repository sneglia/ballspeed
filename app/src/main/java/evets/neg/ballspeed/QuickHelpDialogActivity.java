package evets.neg.ballspeed;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.TextView;


public class QuickHelpDialogActivity extends AppCompatActivity {

    //Activities calling this activity should pass this value as the initiatl parameter to
    //any data passed using putExtra();

    public static final String CODE_TITLE = "1";
    public static final String CODE_FILE_NAME = "2";

    public static boolean isActivityOpened = false;

    //Window fields
    private WebView webView;

    final Context context = this;

    @Override
    protected void onStart() {
        super.onStart();
        isActivityOpened = true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityOpened = false;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Do not close window if dialog is touched outside bounds
        setFinishOnTouchOutside(false);
        setContentView(R.layout.activity_quick_help);

        //Extend help window across whole parent.
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        //Set references to window fields
        webView= (WebView) findViewById(R.id.webQH_QH);

        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        //Based on how this Activity was intended to be started, call appropriate function
        String title = getIntent().getStringExtra(CODE_TITLE);
        String fileName = getIntent().getStringExtra(CODE_FILE_NAME);
        updateTitle(title, null);

        webView.loadUrl("file:///android_asset/" + fileName);
    }

    private void updateTitle(String title, String subTitle) {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);
        actionBar.setSubtitle(subTitle);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                this.finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}

