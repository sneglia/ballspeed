package evets.neg.ballspeed;

//import android.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;


/**
 * Created by Steve on 4/6/2015.
 */
public class MessageBox {

   private int xmlRLayout;
    public static int DEFAULT_MESSAGE_BOX_LAYOUT = R.style.simpleDialogBoxStyle;
    public static int DEFAULT_MESSAGE_BOX_LAYOUT_2 = R.style.simpleDialogBoxStyle2;

    //Display the simplest of message boxes with just an OK button using defalt theme
    public static void ShowOKMessageBox(Context context, String msg, String title) {
        ShowOKMessageBox(context, DEFAULT_MESSAGE_BOX_LAYOUT, msg, title);
    }

    /**********************************************************************************************/
    //Display the simplest of message boxes with just an OK button
    public static void ShowOKMessageBox(Context context, int  xmlRLayout, String msg, String title) {

        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context, xmlRLayout);
        dlgAlert.setMessage(msg);
        dlgAlert.setTitle(title);
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(false);
        dlgAlert.show();
    }
    /**********************************************************************************************/
    // Display message box with OK and Cancel.
    // Upon click, listener callback will get called.

    public static void ShowOKCancelMessageBox(Context context, int  xmlRLayout, String msg, String title, DialogInterface.OnClickListener listener) {

        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(context, xmlRLayout);
        dlgAlert.setMessage(msg);
        dlgAlert.setTitle(title);
        dlgAlert.setPositiveButton("OK", listener);
        dlgAlert.setNegativeButton("Cancel", listener);
        dlgAlert.setCancelable(false);
        dlgAlert.show();

    }

    /**********************************************************************************************/
    //Show up to three buttons with defined text.  Pass in array buttonText[]
    //Order of array is Positive Button, Negative Button, Neutral Button
    public static void ShowMultiButtonMessageBox(Context context, int  xmlRLayout, String msg, String title, String[] buttonText, DialogInterface.OnClickListener listener) {

        if (xmlRLayout == -1)
            xmlRLayout = DEFAULT_MESSAGE_BOX_LAYOUT;

        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(context, xmlRLayout);
        dlgAlert.setMessage(msg);
        dlgAlert.setTitle(title);

        dlgAlert.setPositiveButton(buttonText[0], listener);

        if (buttonText.length > 1)
            dlgAlert.setNegativeButton(buttonText[1], listener);

        if (buttonText.length > 2)
            dlgAlert.setNeutralButton(buttonText[2], listener);

        dlgAlert.setCancelable(false);
        dlgAlert.show();

    }

    /**
     * showOKMessageBoxRequiredFieldString
     *
     * simple method to show OK message box when required fields are missing
     * @param context - context
     * @param requiredFieldString = string representing field missing, example "First Name, Last Name"
     */
    public static void showOKMessageBoxRequiredFieldMissing(Context context, String requiredFieldString) {

        String msg = "Required field(s) not entered: " + requiredFieldString;
        ShowOKMessageBox(context, msg, "Validation Error");
    }

    /**
     * showOKMessageBoxValidationError - simple method to show a validation error
     * @param context context
     * @param validationString string to display
     */
    public static void showOKMessageBoxValidationError(Context context, String validationString) {

        String msg = validationString;
        ShowOKMessageBox(context, msg, "Validation Error");
    }

    private static void changeColorOfDivider(AlertDialog dialog) {

        Context context = dialog.getContext();

        int titleDividerId = context.getResources().getIdentifier("titleDivider", "id", "android");
        View titleDivider = dialog.findViewById(titleDividerId);
        if (titleDivider != null)
            titleDivider.setBackgroundColor(context.getResources().getColor(R.color.standard_text_color_RED));

    }

    /**********************************************************************************************/
    //Display the simplest of message boxes with just an OK button
    public static void showAboutBox(Context context) {

        String msg;

        msg = "Bowling Ball Speed Challenge\n\n" +
                "Version: " + Utils.getAppVersionName(context);

        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context, DEFAULT_MESSAGE_BOX_LAYOUT);
        dlgAlert.setMessage(msg);
        dlgAlert.setTitle("About");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.show();
    }


}
