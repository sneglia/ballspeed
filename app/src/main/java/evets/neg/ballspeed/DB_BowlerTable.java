package evets.neg.ballspeed;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Steve on 6/17/2015.
 *
 *
 * Common FUnction Created:
 *
 *
 *
 */
public class DB_BowlerTable {

    //Table Name
    public static String TABLE_NAME = "Bowlers";

    //BowlingAlley Table Column Name
    public static String COL_ID = "_id";
    public static String COL_FNAME = "FName";
    public static String COL_LNAME= "LName";

    //Handedness
    public static String HANDEDNESS_LEFT = "L";
    public static String HANDEDNESS_RIGHT = "R";

    //GENDER
    public static String GENDER_MALE = "M";
    public static String GENDER_FEMALE = "F";

   //enumerated sort order options that can be passed in
    public enum sortOrder {
        ASCENDING,
        DESCENDING,
        RANDOM
    }

    //holds sorder order;
    private sortOrder so;

    public enum showActiveInactive {
        ACTIVE,
        INACTIVE,
        BOTH
    }
    private showActiveInactive showAI;

    //DBHelper Variable
    private DBHelperBase db;

    //Constructor.  Pass the single dbHelper instance
    public DB_BowlerTable(DBHelperBase db) {

        this.db = db;

        //default sortorder and Active/Inactive
        so = sortOrder.ASCENDING;
        showAI = showActiveInactive.BOTH;

    }

    /**
     *    methods can be used by caller for all default sortOrder of queries
     */
    public void setSortOrder(sortOrder so) {
        this.so = so;
    }


    /**
     *    methods can be used by caller for all default Active/Inactive Stats
     */
    public void setDefaultActiveInactive(showActiveInactive showAI){
        this.showAI = showAI;
    }

     /**
    * getBowlerTable()
    * Returns a List of HashMap objects of bowlers,
    * Calling function should cast to an ArrayList
    */
     public List getBowlersTable()  {

        int cnt = 0;
        List<Map<String, String>> lst = new ArrayList<Map<String, String>>();

        SQLiteDatabase sqlDB = null;
        sqlDB = db.getReadableDatabase();
        String sqlStatement = "select * from " + TABLE_NAME + " ";

        //add order by clause for LName, FirstName, NickName
         switch (so) {
             case ASCENDING:
                 sqlStatement += "order by upper(" + COL_FNAME + ") asc, upper(" + COL_LNAME + ") asc";
                 break;
             case DESCENDING:
                 sqlStatement += "order by upper(" + COL_FNAME + ") desc, upper(" + COL_LNAME + ") desc";
                 break;
         }

        Cursor c = sqlDB.rawQuery(sqlStatement, null);
        c.moveToFirst();

        cnt = c.getCount();

        if (cnt > 0)
        do {

            Map<String, String> map = new HashMap<String, String>();
            map.put(DB_BowlerTable.COL_LNAME, c.getString(c.getColumnIndex(DB_BowlerTable.COL_LNAME)));
            map.put(DB_BowlerTable.COL_FNAME, c.getString(c.getColumnIndex(DB_BowlerTable.COL_FNAME)));
            map.put(DB_BowlerTable.COL_ID, c.getString(c.getColumnIndex(DB_BowlerTable.COL_ID)));

            lst.add(map);

        } while (c.moveToNext());

        c.close();
        sqlDB.close();

        return lst;


    }




    /**
    * getBowlerTableusingID()
      * @param bowlerID
    * Returns a List of HashMap objects of bowlers,
    * Calling function should cast to an ArrayList
    */
     public List getBowlersTableUsingID(int bowlerID)  {

        int cnt = 0;
        List<Map<String, String>> lst = new ArrayList<Map<String, String>>();

        SQLiteDatabase sqlDB = null;
        sqlDB = db.getSQLDB();

        String sqlStatement = "select * from " + TABLE_NAME + " where " + COL_ID + " = " + bowlerID;

        Cursor c = sqlDB.rawQuery(sqlStatement, null);
        c.moveToFirst();

        cnt = c.getCount();

        if (cnt > 0)
        do {

            Map<String, String> map = new HashMap<String, String>();
            map.put(DB_BowlerTable.COL_LNAME, c.getString(c.getColumnIndex(DB_BowlerTable.COL_LNAME)));
            map.put(DB_BowlerTable.COL_FNAME, c.getString(c.getColumnIndex(DB_BowlerTable.COL_FNAME)));
            map.put(DB_BowlerTable.COL_ID, c.getString(c.getColumnIndex(DB_BowlerTable.COL_ID)));

            lst.add(map);

        } while (c.moveToNext());

        c.close();
        db.closeDB();

        return lst;


    }

    /**
     * CheckExistenseBowler
     * @param lname - pass in last name
     * @param fname - pass in first name
     * @param useCaseSensistive -true to look for exact match.  false if case does not matter.
     * @return - List object with hashmap of row
     */
    public List checkExistenceBowler(String lname, String fname, boolean useCaseSensistive) {

        int cnt = 0;
        SQLiteDatabase sqlDB = null;
        String sqlStatement;

        sqlDB = db.getReadableDatabase();
        List<Map<String, String>> lst = new ArrayList<Map<String, String>>();

        String[] args = new String[2];

        if (!useCaseSensistive) {
            sqlStatement = "select * from " + TABLE_NAME + " where Upper(" + COL_LNAME + ") = ? and Upper(" + COL_FNAME + ") = ?";
            args[0] = lname.toUpperCase();
            args[1] = fname.toUpperCase();
        }
        else {
            sqlStatement = "select * from " + TABLE_NAME + " where " + COL_LNAME + " = ? and " + COL_FNAME + " = ?";
            args[0] = lname;
            args[1] = fname;
        }

        Cursor c = sqlDB.rawQuery(sqlStatement, args    );
        c.moveToFirst();
        cnt = c.getCount();

        if (cnt > 0)
            do {

                Map<String, String> map = new HashMap<String, String>();
                map.put(DB_BowlerTable.COL_LNAME, c.getString(c.getColumnIndex(DB_BowlerTable.COL_LNAME)));
                map.put(DB_BowlerTable.COL_FNAME, c.getString(c.getColumnIndex(DB_BowlerTable.COL_FNAME)));
                map.put(DB_BowlerTable.COL_ID, c.getString(c.getColumnIndex(DB_BowlerTable.COL_ID)));

                lst.add(map);

            } while (c.moveToNext());

        c.close();
        sqlDB.close();

        //Log SQL if logging is turned on for debugging
        db.logSQL(sqlStatement, args, getClass().getSimpleName());

        return lst;

}

    /**
     * insertBowlerRecord() - Insert a complete record into the BowlerTable
     *
     * @param lname
     * @param fname
     */
    public void insertBowlerRecord(String lname, String fname) {


        //get DB, either normal or transaction
        SQLiteDatabase sqlDB = db.getSQLDB();

        ContentValues initialValues = new ContentValues();

        initialValues.put(COL_LNAME, lname);
        initialValues.put(COL_FNAME, fname);

        sqlDB.insert(TABLE_NAME, null, initialValues);

        //close the DB if not in transaction mode
        db.closeDB();

    }


    /**
     * Update the bowler record for the passed ID. I
     * @param _id Record ID for bowling alley.  Required
     * @param lname - last name of bowler.  null is acceptable.  It will not be updated
     * @param fname - first name of bowler.  null is acceptable.
     */
public void updateBowlerRecord(int _id, String lname, String fname) {

    //get DB, either normal or transaction
    SQLiteDatabase sqlDB = db.getSQLDB();

    ContentValues contentValues = new ContentValues();

    if (lname != null)
        contentValues.put(COL_LNAME, lname);

    if (fname != null)
        contentValues.put(COL_FNAME, fname);

    String whereClause = COL_ID + "= ?";
    String[] args = {String.valueOf(_id)};

    sqlDB.update(TABLE_NAME, contentValues, whereClause, args);

    //close the DB if not in transaction mode
    db.closeDB();

    }

    /**
     * Delete the bowler for the passed _id.
     * @param _id Record ID for bowling alley.  Required
     */
public void deleteBowlerRecord(int _id) {

    //get DB, either normal or transaction
    SQLiteDatabase sqlDB = db.getSQLDB();

    ContentValues contentValues = new ContentValues();

    String whereClause = COL_ID + "= ?";
    String[] args = {String.valueOf(_id)};

    sqlDB.delete(TABLE_NAME, whereClause, args);

    //close the DB if not in transaction mode
    db.closeDB();

    }


}