package evets.neg.ballspeed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    //Window fields
    final Context context = this;

    //Distances from calculation points
    private static final int DISTANCE_DEFAULT = 60;
    private static final int DISTANCE_ARROWS = 43;
    private static final int DISTANCE_DOTS= 54;

    private String[][] distanceOptions = {{"Distance from foul line to head pin (60 feet)", DISTANCE_DEFAULT+""},
                                          {"Distance from guide dots to head pin (54 feet)", DISTANCE_DOTS+""},
                                          {"Distance from middle arrow to head pin (43 feet)", DISTANCE_ARROWS+""}};

    private int hits, misses;

    private TextView txtChron;
    private TextView txtBallSpeedMPH;
    private TextView txtBallSpeedMetersPS;
    private TextView txtAllowedDeviation;
    private TextView txtAllowedDeviationMPH;
    private TextView txtTargetSpeedHit;
    private TextView txtTargetSpeedMiss;
    private TextView txtBowlers;
    private Spinner lstDistanceOptions;
    private Spinner lstBowlers;
    private EditText edtTargetSpeed;
    private EditText edtAllowedDeviation;
    private ImageView imgStartStop;
    private RelativeLayout RL_BDSS_Banner;

    private int distanceSelected;
    private int distanceLocation;
    private int bowlerLocation;
    private int bowlerID;
    private String lastSeconds;

    BallSpeed bs;

    //Timer for user interface;
    UITimer timer;

    //Firebase
    private FirebaseAnalytics mFirebaseAnalytics;

    Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FirebaseCrash.log("In MainActivity / OnCreate");
        //Do not close window if dialog is touched outside bounds
        //setFinishOnTouchOutside(false);
        //supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_ball_speed);

        //Instantiate Firebase Analytics
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Reference to fields
        txtChron = (TextView) findViewById(R.id.txtChron_SPEED);
        txtBallSpeedMPH = (TextView) findViewById(R.id.txtBallMPH_SPEED);
        txtBallSpeedMetersPS = (TextView) findViewById(R.id.txtBallMetersPS_SPEED);
        lstDistanceOptions = (Spinner) findViewById(R.id.lstCalculateBallSpeed);
        lstBowlers = (Spinner) findViewById(R.id.lstSaveName);
        txtBowlers = (TextView) findViewById(R.id.txtSaveName);
        edtTargetSpeed = (EditText) findViewById(R.id.edtTargetSpeed);
        edtAllowedDeviation = (EditText) findViewById(R.id.edtTargetSpeedDeviation);
        txtAllowedDeviation = (TextView) findViewById(R.id.txtTargetSpeedDeviation);
        txtAllowedDeviationMPH = (TextView) findViewById(R.id.txtTargetSpeedDeviationMPH);
        txtTargetSpeedHit = (TextView) findViewById(R.id.txtTargetsSpeedHit);
        txtTargetSpeedMiss = (TextView) findViewById(R.id.txtTargetsSpeedMisses);
        RL_BDSS_Banner = (RelativeLayout) findViewById(R.id.RL_BDSS_Banner);

        imgStartStop = (ImageView) findViewById(R.id.imgStartStop_SPEED);


        //BallSpeed global object
        bs = new BallSpeed((Activity) context, DISTANCE_DEFAULT);

        // Create an instance of the Ballspeed object.  It will call UpdateTimer as the timer hits it's interval.

        //last parameter is the distance the ball will travel
        final DecimalFormat myFormatter = new DecimalFormat("000");
        bs.setOnUpdateTimer(new BallSpeed.OnUpdateTimer() {
            @Override
            public void UpdateTimer(long hour, long minute, long seconds, long milliseconds) {

                if (minute > 0) {
                    timerStop();
                    MessageBox.ShowOKMessageBox(context, "Process aborted.", "Information");
                    txtBallSpeedMPH.setText("--");
                    txtBallSpeedMetersPS.setText("--");
                    enableFields(true);
                    Utils.lockScreenOrientation((Activity)context, false);
                }
                else {
                    lastSeconds = seconds + "." + myFormatter.format(milliseconds);
                    txtChron.setText(seconds + "." + myFormatter.format(milliseconds) + " seconds");
                }
            }
        });

        final Activity act = this;

        //Handle enable and disable of deviation field if user does not select Target Speed
        edtTargetSpeed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 0) {
                    edtAllowedDeviation.setEnabled(true);
                    txtAllowedDeviation.setEnabled(true);
                    txtAllowedDeviationMPH.setEnabled(true);
                }
                else {
                    edtAllowedDeviation.setEnabled(false);
                    txtAllowedDeviation.setEnabled(false);
                    txtAllowedDeviationMPH.setEnabled(false);
                }
            }
        });

        //Handle when distance is clicked
        lstDistanceOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (view == null)
                    return;

                    distanceLocation = position;
                    distanceSelected =  Integer.parseInt(((TextView) view.findViewById(R.id.txtCodeG1)).getText().toString());
                    bs.setDistance(distanceSelected);
                    updateTitle(getResources().getString(R.string.app_name_full), "Measure Ball Speed At " + bs.getDistance() + " Feet");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Handle when distance is clicked
        lstBowlers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (view == null)
                    return;

                    bowlerLocation = position;
                    bowlerID =  Integer.parseInt(((TextView) view.findViewById(R.id.txtCodeG1)).getText().toString());

                    // A bowler was selected so reset
                    if (bowlerID > 0) {
                        hits = misses = 0;
                        txtTargetSpeedHit.setText(hits + "");
                        txtTargetSpeedMiss.setText(misses + "");
                    }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //Start and stop the ballspeed timer.
        imgStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //start the timer.  have it return back to the activity every 50 milliseconds for updating
                if (!bs.isTimerRunning()) {
                    bs.startTimer(50);
                    imgStartStop.setImageResource(R.drawable.stop_button);
                    txtBallSpeedMPH.setText("--");
                    txtBallSpeedMetersPS.setText("--");
                    updateTitle(getResources().getString(R.string.app_name_full), "Measure Ball Speed At " + bs.getDistance() + " Feet");
                    enableFields(false);
                    Utils.lockScreenOrientation((Activity)context, true);

                    //if a ballspeed is entered and a deviation is not, assume zero
                    if (edtTargetSpeed.getText().length() > 0 && edtAllowedDeviation.getText().toString().length() == 0)
                        edtAllowedDeviation.setText("0");
                } else {

                    timerStop();

                    enableFields(true);
                    Utils.lockScreenOrientation((Activity)context, false);

                    //Assume thisis not a Hit or Miss scenario
                    String isHit = null;

                    //calculate Hit or Miss if a target speed was identified
                    if (edtTargetSpeed.getText().toString().length() > 0)
                        isHit = calculateHitMiss();

                    //If a bowler was identified,save data
                    if (bowlerID != 0)
                        saveData(isHit);
                }
            }
        });

        RL_BDSS_Banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleBannerClick();
            }
        });

        //Initialize key variables
        hits = misses = 0;
        distanceSelected = 0;
        distanceLocation = 0;

        //Save Instance state variables;
        if (savedInstanceState != null) {

            hits = savedInstanceState.getInt("hits");
            misses = savedInstanceState.getInt("misses");
            distanceSelected = savedInstanceState.getInt("distanceSelected");
            distanceLocation = savedInstanceState.getInt("distanceLocation");
            bowlerID = savedInstanceState.getInt("bowlerID");
            bowlerLocation = savedInstanceState.getInt("bowlerLocation");
            txtBallSpeedMPH.setText(savedInstanceState.getString("mph"));
            txtChron.setText(savedInstanceState.getString("chron"));
        }

    }

    /**
     * Save all variables needing restoration later
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Saving variables
        savedInstanceState.putInt("distanceSelected", distanceSelected);
        savedInstanceState.putInt("distanceLocation", distanceLocation);
        savedInstanceState.putInt("bowlerID", bowlerID);
        savedInstanceState.putInt("bowlerLocation", bowlerLocation);
        savedInstanceState.putInt("hits", hits);
        savedInstanceState.putInt("misses", misses);
        savedInstanceState.putString("mph", txtBallSpeedMPH.getText().toString());
        savedInstanceState.putString("chron", txtChron.getText().toString());

        // Call at the end
        super.onSaveInstanceState(savedInstanceState);
    }


    /**
     * Calculate whether the ball speed was +/- the target speed and speed deviation.
     * If so, increment the hit or miss counter
     * @return String Y or N for a hit or miss
     */
    private String calculateHitMiss() {

        float targetSpeed = 0;
        float deviation = 0;
        float ballSpeed = Float.parseFloat(txtBallSpeedMPH.getText().toString());
        String isHit;

        targetSpeed = Float.parseFloat(edtTargetSpeed.getText().toString());

        if (edtTargetSpeed.getText().toString().length() > 0) {
            deviation = Float.parseFloat(edtAllowedDeviation.getText().toString());
        }

        CustomToast customToast = new CustomToast();
        customToast.setGravity(Gravity.BOTTOM, 0, CustomToast.CUSTOM_TOAST_GRAVITY);

        if ((ballSpeed >= targetSpeed - deviation) && (ballSpeed <= targetSpeed + deviation)) {
            hits++;
            txtTargetSpeedHit.setText(hits+"");
            customToast.toast(this, R.layout.custom_toast_layout_success_hit, R.id.textToShow, "Target Speed Hit!");
            isHit = DB_HistoryTable.IS_HIT_YES;

        }
        else {
            misses++;
            txtTargetSpeedMiss.setText(misses + "");
            customToast.toast(this, R.layout.custom_toast_layout_miss_hit, R.id.textToShow, "Target Speed Missed");
            isHit = DB_HistoryTable.IS_HIT_NO;
        }

        return isHit;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            //help was clicked
            case R.id.menu_quick_help:

                Intent i = new Intent(this, QuickHelpDialogActivity.class);
                //i.putExtra(QuickHelpDialogActivity.MODE_MESSAGE_CODE, QuickHelpDialogActivity.START_MODE_MEASURE_BALL_SPEED);
                i.putExtra(QuickHelpDialogActivity.CODE_TITLE, "Ball Speed Help");
                i.putExtra(QuickHelpDialogActivity.CODE_FILE_NAME, "qh_measure_ball_speed.htm");
                startActivity(i);
                break;

            case R.id.menu_add_edit_user:

                startActivity(new Intent(this, BowlerActivity.class));
                break;

            case R.id.menu_show_history:

                startActivity(new Intent(this, HistoryActivity.class));
                break;

            case R.id.menu_about:

                MessageBox.showAboutBox(this);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Update title bar
     * @param title
     * @param subTitle
     */
    public void updateTitle(String title, String subTitle) {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);
        actionBar.setSubtitle(subTitle);
    }


            /**
             * Common function to call when the timer is stopped.
             */
        private void timerStop() {

        bs.stopTimer();
        imgStartStop.setImageResource(R.drawable.start_button);

            //For test only...India locale where 12.50 will be represented as 12,56
            //Locale locale = new Locale("in", "IN");
            // here we update locale for date formatters
            //Locale.setDefault(locale);

            //Default locale to US as 12.50
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat myFormatter = (DecimalFormat)nf;
            myFormatter.applyPattern("##.00");

        //DecimalFormat myFormatter = new DecimalFormat("##.00");
        txtBallSpeedMPH.setText(myFormatter.format(bs.getBallSpeedMilesPerHour()));
        txtBallSpeedMetersPS.setText(myFormatter.format(bs.getBallSpeedKilometersPerHour()));
    }

    @Override
    public void onResume() {

        super.onResume();

        String[] distanceOption = new String[distanceOptions.length];
        String[] distanceLength = new String[distanceOptions.length];

        for(int i=0; i< distanceOptions.length; i++) {
            distanceOption[i] = distanceOptions[i][0];
            distanceLength[i] = distanceOptions[i][1];
        }


        //Load the listview adapter with Mainfield and hiddene elemnsts
        AdapterGenericWithCodes adapter = new AdapterGenericWithCodes(this, distanceOption, distanceLength, null, null, null, null, null, null, null, null, null, R.layout.list_row_generic_10_codes);
        adapter.addHeaderRow("Calculate Ball Speed Using", R.layout.list_row_generic_codes_group_header_row);
        lstDistanceOptions.setAdapter(adapter);

        //Increment distance location to skip header
        if (distanceLocation == 0)
            distanceLocation++;

        //Start at first option in array, but skip header
        lstDistanceOptions.setSelection(distanceLocation);

        txtTargetSpeedHit.setText(hits+"");
        txtTargetSpeedMiss.setText(misses+"");

        //Load all the bowlers
        loadBowlers();

        //If BDSS is installed, hide Banner
        try {
                //Look for the package.  If it is not found, an exception will be thrown and the banner will be shown
                getPackageManager().getPackageInfo(getResources().getString(R.string.BDSS_PACKAGE), 0);
                RL_BDSS_Banner.setVisibility(View.GONE);

            } catch (Exception e) {
                RL_BDSS_Banner.setVisibility(View.VISIBLE);

        }


    }

    @Override
    public void onWindowFocusChanged (boolean hasFocus) {



    }

    /**
     * Load the Bowlers Spinner
     */
    private void loadBowlers() {

        String[] lname = null;
        String[] fname = null;
        String[] fullName = null;
        String[] ID = null;
        int cnt = 0;
        boolean flBowlerIDFound = false;

        //Attach to DB
        DB_BowlerTable sqlDB = new DB_BowlerTable(DBhelper.getInstance(this));

        //Execute query
        ArrayList<HashMap<String, String>> baTable = (ArrayList<HashMap<String, String>>) sqlDB.getBowlersTable();

        //For each row of the return ArrayList, extract the Map object and pull out data into arrays
        if (baTable != null && baTable.size() > 0) {

            cnt = baTable.size();

            //Make room for all returned rows and 1 empty row for No Bowler
            lname = new String[cnt+1];
            fname = new String[cnt+1];
            fullName = new String[cnt+1];
            ID = new String[cnt+1];

            for (int i = 0; i < cnt; i++) {
                HashMap<String, String> map = baTable.get(i);
                lname[i] = map.get(DB_BowlerTable.COL_LNAME);
                fname[i] = map.get(DB_BowlerTable.COL_FNAME);
                ID[i] = map.get(DB_BowlerTable.COL_ID);

                //minimum field required
                fullName[i] = fname[i];

                //did they supply a last name, then include
                if (lname[i] != null && lname[i].length() !=0)
                    fullName[i] += " " + lname[i];

                //Is the current bowler ID in the list.  It may ot be if it was deleted
                if (bowlerID == Integer.parseInt(ID[i])) {
                    flBowlerIDFound = true;
                }
            }
        }

        //If the current bowler is no longer found, initialize it.
        if (!flBowlerIDFound) {
            bowlerID = 0;
            bowlerLocation = 0;
        }

        //Fill Empty Row
        if (cnt == 0) {
            fullName = new String[1];
            ID = new String[1];
            fullName[0] = "<No available bowlers>";
            ID[0] = "0";
        }
        else {
            fullName[cnt] = "<No bowler>";
            ID[cnt] = "0";
        }

        //Load the listview adapter with Mainfield and hiddene elemnsts
        AdapterGenericWithCodes adapter = new AdapterGenericWithCodes(this, fullName, ID, null, null, null, null, null, null, null, null, null, R.layout.list_row_generic_10_codes);
        lstBowlers.setAdapter(adapter);
        adapter.addHeaderRow("Capture Ball Speed For", R.layout.list_row_generic_codes_group_header_row);
        adapter.notifyDataSetChanged();

        //If we found bowlers, select last one selected if we have one
        if (cnt != 0) {

            //select last bowler selected, else empty row
            if (bowlerID != 0) {

                //lstBowlers.setSelection(bowlerLocation);

                //Find the new name in the list
                adapter = (AdapterGenericWithCodes) lstBowlers.getAdapter();
                int pos = adapter.getItem(bowlerID+"", AdapterGenericWithCodes.GroupBy.Code1);
                if (pos != AdapterGenericWithCodes.ITEM_NOT_FOUND)
                    lstBowlers.setSelection(pos);

            }
            else
                lstBowlers.setSelection(cnt+1);
        }

        //There's no beolwers, so select the empty row
        else {
            adapter.setPositionState(1,true, false);
            lstBowlers.setSelection(1);
        }


    }

    /**
     * Save historical data
     * @param isHit Y, N or null if this was not a Hit or Miss scenario
     */
    private void saveData(String isHit) {

        //Make sure this name does not exist
        DBhelper db = DBhelper.getInstance(this);
        DB_HistoryTable sqlDB = new DB_HistoryTable(db);

        //INsert row.  If target speed was not entered, pass null value
        sqlDB.insertHistoryRecord(bowlerID,
                                 (edtTargetSpeed.getText().toString().length() == 0) ? DBHelperBase.SET_AS_NULL_VALUE: Float.parseFloat(edtTargetSpeed.getText().toString()),
                                 (edtTargetSpeed.getText().toString().length() == 0) ? DBHelperBase.SET_AS_NULL_VALUE : Float.parseFloat(edtAllowedDeviation.getText().toString()),
                                 Float.parseFloat(lastSeconds),
                                 Float.parseFloat(txtBallSpeedMPH.getText().toString()),
                                 Float.parseFloat(txtBallSpeedMetersPS.getText().toString()),
                                 isHit);


    }

    /**
     *
     * @param enable - true to enable, false to disable
     */
    private void enableFields(boolean enable) {

        lstBowlers.setEnabled(enable);
        lstDistanceOptions.setEnabled(enable);
        edtTargetSpeed.setEnabled(enable);
        edtAllowedDeviation.setEnabled(enable);
        menu.findItem(R.id.menu_add_edit_user).setEnabled(enable);
        menu.findItem(R.id.menu_show_history).setEnabled(enable);


    }


    /**
     * When user clicks banner, take them to Google Play and capture firebase analytics
     */
    private void handleBannerClick() {



        //Get BDSS Package and start Google Play
        final String appPackageName = getResources().getString(R.string.BDSS_PACKAGE); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

        //track event in Firebase
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "BDSSBannerImageView");
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "BDSSBanner");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "BDSSBannerClicked");

        //Track custom date and time when clicked.
        bundle.putString("BDSSBannerClick_DateTime", Utils.getCurrentDate() + " " + Utils.getCurrentTime());
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);


    }

    /**
     * OnDestroy
     */
    public void onDestroy() {

        super.onDestroy();

        //If the timer is running, stop it or an error will occur
        if (bs.isTimerRunning()) {

            timerStop();
        }
    }



}


