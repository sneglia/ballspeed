package evets.neg.ballspeed;

/**
 * Created by Steve on 5/21/2015.
 *
 * DBHelper is the database helper class used for single transaction processing.  It does not support multiple transaction processing
 * Use DBHelperTransaction if true transactions are called.
 * To use, do something like:
 *
     //Attach to DB
    DB_BowlerTable sqlDB = new DB_BowlerTable(DBhelper.getInstance(getActivity()));

     //Execute query
    ArrayList<HashMap<String, String>> baTable = (ArrayList<HashMap<String, String>>) sqlDB.getBowlersTable();
 *
 *
 */


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DBhelper extends DBHelperBase {

    private static DBhelper mInstance;
    private SQLiteDatabase sqlDB;

    private static Context mContext;

    //This should not be called implicitely which is why its private.
    // Use getInstance to get an instance of this object.
     private DBhelper(Context context) {

        super(context);
        sqlDB = null;
    }

    //This should not be called implicitely which is why its private.
    // Use getInstance to get an instance of this object.
    //This will be called when needing a specific database name
     private DBhelper(Context context, String dbName) {

        super(context, dbName);
        sqlDB = null;
    }

    //Call this method to return an instance of DBHelper.  This ensure only once instance will
    //ever be created.
    public static DBhelper getInstance(Context context) {

        if (mInstance == null) {
            mInstance = new DBhelper(context);
        }

        mContext = context;

        return mInstance;
    }

    //return the context (Activity) of the calling function
    public static Context getContext() {
        return mContext;
    }

    //Call this method to return an instance of DBHelper.
    //Use this version for a specific database name.
    public static DBhelper getInstance(Context context, String dbName) {

            return new DBhelper(context, dbName);
    }

    /**
     *
     * @return false here.  DBHelper is for single processing only, not transactions.
     */
    public boolean inTransactionMode() {
        return false;
    }


    /**
     * return a SQLLite DB for writing when we are not in transaction mode
     * @return
     */
    public SQLiteDatabase getSQLDB() {

        if (sqlDB == null)
            sqlDB = getWritableDatabase();

        return sqlDB;
    }

    /**
     * close the database that is passed it.  typically this would be returned get from getSQLDB()
     */
        public void closeDB() {

            sqlDB.close();
            sqlDB = null;



    }

}

